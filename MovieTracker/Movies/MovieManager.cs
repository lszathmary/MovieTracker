﻿using System;
using System.Collections.Generic;
using MovieTracker.DataBase;
using System.Drawing;
using System.Data;
using System.IO;
using System.Drawing.Imaging;
using MovieTracker;
using MovieTracker.Utils;


namespace MovieTracker.Movies
{
    /// <summary>
    /// MovieCollection class.
    /// </summary>
    public class MovieCollection
    {
        // Resposanble for reading and writing date from/to the database.
        private DataBaseManager dataBaseManager;

        // The directory containing the posters and previously generated thumbnails
        private String directory;
                
        /// <summary>
        /// Constructor.
        /// </summary>
        public MovieCollection()
        {
            
            this.dataBaseManager = new DataBaseManager();

            // Set directory containing the posters and thumbnails previously generated.
            this.directory = Directory.GetCurrentDirectory() + @"\collection\posters\";
        }
        
        /// <summary>
        /// Gets the movie with the specified id from the collection.
        /// </summary>
        public Movie GetMovie(int id)
        {
            var movie = new Movie();
                       
            // Extract movie information with the specified id from the database.
            DataTable dataTable = dataBaseManager.GetMovie(id);
            
            // Get the movie information from the datatable.
            movie.Id = int.Parse(dataTable.Rows[0]["Id"].ToString());
            movie.Title = dataTable.Rows[0]["Title"].ToString();
            movie.Type = int.Parse(dataTable.Rows[0]["Type"].ToString());

            movie.Genre = dataTable.Rows[0]["Genre"].ToString();
            movie.Director = dataTable.Rows[0]["Director"].ToString();
            movie.Cast = dataTable.Rows[0]["Cast"].ToString();
            movie.Description = dataTable.Rows[0]["Description"].ToString();
            movie.Rating = Decimal.Parse(dataTable.Rows[0]["Rating"].ToString());
            movie.Year = Convert.StringToInt(dataTable.Rows[0]["Year"].ToString());

            movie.URL = dataTable.Rows[0]["URL"].ToString();
            movie.PosterFileName = dataTable.Rows[0]["PosterFileName"].ToString();
            movie.ThumbnailFileName = dataTable.Rows[0]["ThumbnailFileName"].ToString();
            
            // Load the poster and the thumbnail from the disk.
            movie.Poster = this.LoadPosterFromFile(movie.PosterFileName);
            movie.Thumbnail = this.LoadThumbnailFromFile(movie.ThumbnailFileName);
            
            return movie;
        }

        /// <summary>
        /// Return a list with all the movies in the collection.
        /// </summary>
        public List<Movie> GetMovies()
        {
            var movieList = new List<Movie>();

            // Extract all the movies from the database.
            DataTable dataTable = dataBaseManager.GetMovies();
            
            // Each row contains information about a different movie.
            foreach (DataRow dataRow in dataTable.Rows)
            {
                var movie = new Movie();

                // Get the movie information from the datatable.
                movie.Id = int.Parse(dataRow["Id"].ToString());
                movie.Title = dataRow["Title"].ToString();
                movie.Type = int.Parse(dataRow["Type"].ToString());

                movie.Genre = dataRow["Genre"].ToString();
                movie.Director = dataRow["Director"].ToString();
                movie.Cast = dataRow["Cast"].ToString();
                movie.Description = dataRow["Description"].ToString();
                movie.Rating = Decimal.Parse(dataRow["Rating"].ToString());
                movie.Year = Convert.StringToInt(dataRow["Year"].ToString());

                movie.URL = dataRow["URL"].ToString();
                movie.PosterFileName =  dataRow["PosterFileName"].ToString();
                movie.ThumbnailFileName = dataRow["ThumbnailFileName"].ToString();

                // Load the poster and the thumbnail from the disk.
                movie.Poster = this.LoadPosterFromFile(movie.PosterFileName);
                movie.Thumbnail = this.LoadThumbnailFromFile(movie.ThumbnailFileName);

                // Add the movie to the list.
                movieList.Add(movie);
            }

            return movieList;
        }

        /// <summary>
        /// Returns the list with all the movies that match the spefified criteria.
        /// </summary>
        public List<Movie> SearchMovies(String title, String genre, String director, int year)
        {
            var movieList = new List<Movie>();

            // Extract all the movies from the database.
            DataTable dataTable = dataBaseManager.SearchMovies(title, genre, director, year);

            // Each row contains information about a different movie.
            foreach (DataRow dataRow in dataTable.Rows)
            {
                var movie = new Movie();

                // Get the movie information from the datatable.
                movie.Id = int.Parse(dataRow["Id"].ToString());
                movie.Title = dataRow["Title"].ToString();
                movie.Type = int.Parse(dataRow["Type"].ToString());

                movie.Genre = dataRow["Genre"].ToString();
                movie.Director = dataRow["Director"].ToString();
                movie.Cast = dataRow["Cast"].ToString();
                movie.Description = dataRow["Description"].ToString();
                movie.Rating = Decimal.Parse(dataRow["Rating"].ToString());
                movie.Year = Convert.StringToInt(dataRow["Year"].ToString());

                movie.URL = dataRow["URL"].ToString();
                movie.PosterFileName = dataRow["PosterFileName"].ToString();
                movie.ThumbnailFileName = dataRow["ThumbnailFileName"].ToString();

                // Load the poster and the thumbnail from the disk.
                movie.Poster = this.LoadPosterFromFile(movie.PosterFileName);
                movie.Thumbnail = this.LoadThumbnailFromFile(movie.ThumbnailFileName);

                // Add the movie to the list.
                movieList.Add(movie);
            }

            return movieList;
        }

        /// <summary>
        /// Add a new movie to the collection.
        /// </summary>
        public Movie AddMovie(String title, int type, String genre, String director, String cast, 
                    String description, Decimal rating, int year, String url, Image poster)
        {
            // Insert the movie information into the DataBase.
            int id = dataBaseManager.InsertMovie(title, type, genre, director, cast, description, rating, year, url);

            if (poster != null)
            {
                // Generate thumbnail from poster.
                Image thumbnail = Utility.CreateThumbnail(poster, 110, 150, 3);

                // Save the poster and the generated thumbnail to the disk.
                poster.Save(this.directory + id.ToString() + ".png", ImageFormat.Png);
                thumbnail.Save(this.directory + id.ToString() + "_tb.png", ImageFormat.Png);

                // Update the Poster filename and Thumbnail filename of this movie in the database,
                // the filenames contain the id number of the movie, so we have to do a separted update
                // because we did not know the id was going to be before the insertion.
                dataBaseManager.UpdateFileName(id, id.ToString() + ".png", id.ToString() + "_tb.png");
            }

            return this.GetMovie(id);
        }

        /// <summary>
        /// Update the movie in the collection with the specified id.
        /// </summary>
        public void UpdateMovie(int id, String title, int type, String genre, String director, String cast, 
                        String description, Decimal rating, int year, String url, Image poster)
        {
            var movie = this.GetMovie(id);  // The movie that has to be updated.
          
            // Update the movie information in the database.
            dataBaseManager.UpdateMovie(id, title, type, genre, director, cast, description, rating, year, url);

            // Delete the old poster and thumbnail if there are present.
            if (movie.Poster != Properties.Resources.Poster) this.DeleteFileFromDisk(movie.PosterFileName);
            if (movie.Thumbnail != Properties.Resources.Thumbnail) this.DeleteFileFromDisk(movie.ThumbnailFileName);

            if (poster != null)
            {
                // Generate thumbnail from poster image.
                Image thumbnail = Utility.CreateThumbnail(poster, 110, 150, 3);
                
                // Save the poster and the generated thumbnail to the disk.
                poster.Save(this.directory + id.ToString() + ".png", ImageFormat.Png);
                thumbnail.Save(this.directory + id.ToString() + "_tb.png", ImageFormat.Png);

                // Update the Poster filename and Thumbnail Filename of the movie in the database
                dataBaseManager.UpdateFileName(id, id.ToString() + ".png", id.ToString() + "_tb.png");
            }

        }

        /// <summary>
        /// Delete the movie with the specified id from the collection.
        /// </summary>
        public void DeleteMovie(int id)
        {
            var movie = this.GetMovie(id);

            // Delete the poster and thumbnail from the disk.
            if (movie.Poster != Properties.Resources.Poster)  this.DeleteFileFromDisk(movie.PosterFileName);
            if (movie.Thumbnail != Properties.Resources.Thumbnail) this.DeleteFileFromDisk(movie.ThumbnailFileName);

            // Delete the movie from the database.
            dataBaseManager.DeleteMovie(id);
        }
                
        /// <summary>
        /// Load the thumbnail from the disk or set reference to the dummy image 
        /// if the specified file does not exists.
        /// </summary>
        private Image LoadThumbnailFromFile(String fileName)
        {
            if (!File.Exists(this.directory + fileName))
            {
                // In case the file does not exist return a DummyThumbnail.
                return Properties.Resources.Thumbnail;
            }
            else
            {

                // NOTE:
                //  Do not used Image.FromFile() beacuse the file 
                //  located on the disk will get locked and
                //  can not be modified or edited.
                using (Stream stream = File.OpenRead(this.directory + fileName))
                {
                    return System.Drawing.Image.FromStream(stream);
                }
           }
        }

        /// <summary>
        /// Load the poster from the disk or set reference to the dummy image if 
        /// the specified file does not exists.
        /// </summary>
        private Image LoadPosterFromFile(String fileName)
        {
            if (!File.Exists(this.directory + fileName))
            {
                // In case the file does not exist return a DummyPoster.
                return Properties.Resources.Poster;
            }
            else
            {
                // NOTE:
                //  Do not used Image.FromFile() beacuse the file 
                //  located on the disk will get locked and
                //  can not be modified or edited.
                using (Stream stream = File.OpenRead(this.directory + fileName))
                {
                    return System.Drawing.Image.FromStream(stream);
                }
            }
        }

        /// <summary>
        /// Delete the specified file from the disk.
        /// </summary>
        /// <param name="fileName"></param>
        private void DeleteFileFromDisk(String fileName)
        {
            // Delete the file if it exists.
            if (File.Exists(this.directory + fileName))
            {
                File.Delete(this.directory + fileName);
            }
        }

    }
}
