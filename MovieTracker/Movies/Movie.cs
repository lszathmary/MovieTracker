﻿using System;
using System.Drawing;

namespace MovieTracker.Movies
{
    /// <summary>
    /// Movie class.
    ///     Contains information about a specific movie in the collection.
    /// </summary>
    public class Movie
    {
        private int id;
        private String title;
        private int type;

        private String director;
        private String genre;
        private String description;
        private String cast;
        private int year;
        private Decimal rating;

        private String url;
        
        private String posterFileName;
        private String thumbnailFileName;

        private Image poster;
        private Image thumbnail;

        /// <summary>
        /// Constructor.
        /// NOTE:
        ///  This class can only be created and modified by the MovieCollection class.
        ///  Do not try modify it directly the changes will not be saved to the database.
        /// </summary>
        internal Movie()
        {
            this.id = -1;
            this.title = "";
            this.director = "";
            this.genre = "";
            this.year = 0;
        }

        /// <summary>
        /// The id of the movie.
        /// </summary>
        public int Id
        {
            get
            {
                return this.id;
            }    
            internal set
            {
               this.id = value;
            }
        }

        /// <summary>
        /// The title of the movie.
        /// </summary>
        public String Title
        {
            get
            {
                return this.title;
            }
            internal set
            {
                this.title = value;
            }
        }
        
        /// <summary>
        ///  The type of the movie.
        /// </summary>
        public int Type
        {
            get
            {
                return this.type;
            }
            internal set
            {
                this.type = value;
            }
        }

        /// <summary>
        /// The director of the movie.
        /// </summary>
        public String Director
        {
            get
            {
                return this.director;
            }
            internal set
            {
                this.director = value;
            }
        }

        /// <summary>
        /// The genre of the movie.
        /// </summary>
        public String Genre
        {
            get
            {
                return this.genre;
            }
            internal set
            {
                this.genre = value;
            }
        }

        /// <summary>
        /// The cast of the movie.
        /// </summary>
        public String Cast
        {
            get
            {
                return this.cast;
            }
            internal set
            {
                this.cast = value;
            }
        }

        /// <summary>
        /// The description of the movie.
        /// </summary>
        public String Description
        {
            get
            {
                return this.description;
            }
            internal set
            {
                this.description = value;
            }
        }
        
        /// <summary>
        /// The rating of the movie.
        /// </summary>
        public Decimal Rating
        {
            get
            {
                return this.rating;
            }
            internal set
            {
                this.rating = value;
            }
        }

        /// <summary>
        /// The year the movie was released.
        /// </summary>
        public int Year
        {
            get
            {
                return this.year;
            }
            internal set
            {
                this.year = value;
            }
        }
        
        /// <summary>
        /// The url from wich the information about the movie was downloaded.
        /// </summary>
        public String URL
        {
            get
            {
                return this.url;
            }
            internal set
            {
                this.url = value;
            }
        }
        
        /// <summary>
        /// The name of the file containing the poster.
        /// </summary>
        public String PosterFileName
        {
            get
            {
                return this.posterFileName;
            }
            internal set
            {
                this.posterFileName = value;
            }
        }
        
        /// <summary>
        /// The name of the file containing the thumbnail.
        /// </summary>
        public String ThumbnailFileName
        {
            get
            {
                return this.thumbnailFileName;
            }
            internal set
            {
                this.thumbnailFileName = value;
            }
        }

        /// <summary>
        /// The poster of the movie.
        /// </summary>
        public Image Poster
        {
            get
            {
                return this.poster;
            }
            internal set
            {
                this.poster = value;
            }
        }

        /// <summary>
        /// The thumbnail generated from the poster of the movie.
        /// </summary>
        public Image Thumbnail
        {
            get
            {
                return this.thumbnail;
            }
            internal set
            {
                this.thumbnail = value;
            }
        }

    }
}
