﻿using MovieTracker.Movies;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MovieTracker.UserInterface
{
    public partial class FormSearchMovie : Form
    {
        // Responsable for managing the collection of movies.
        private MovieListView movieListView = null;

        public FormSearchMovie()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public void bind(MovieListView movieListView)
        {
            this.movieListView = movieListView;
        }


        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                movieListView.SearchMovies(tBoxTitle.Text, "", "", 0);
            }

            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void FormSearchMovie_FormClosed(object sender, FormClosedEventArgs e)
        {
            Movie movie = movieListView.GetSelectedMovie();

            movieListView.LoadMovies();

            if (movie != null)
            {
                movieListView.SelectMovie(movie);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            movieListView.SearchMovies(tBoxTitle.Text, "", "", 0);
        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

    }
}
