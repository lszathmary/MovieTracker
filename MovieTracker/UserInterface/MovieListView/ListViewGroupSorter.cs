﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace MovieTracker
{
    /// <summary>
    /// Exposes a method that compares two groups.
    /// Note the header text of the groups that are compared must contain the release year of a movie.
    /// </summary>
    public class ListViewGroupSorter : IComparer<ListViewGroup>
    {
        private SortOrder sortOrder;

        /// <summary>
        /// Constuctor.
        /// </summary>
        public ListViewGroupSorter(SortOrder sortOrder)
        {
            this.sortOrder = sortOrder;
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        public int Compare(ListViewGroup group1, ListViewGroup group2)
        {
            if (int.Parse(group1.Header) > int.Parse(group2.Header))
            {
                if (this.sortOrder == SortOrder.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            if (int.Parse(group1.Header) < int.Parse(group2.Header))
            {
                if (this.sortOrder == SortOrder.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }

            return 0;
        }
    }
}
