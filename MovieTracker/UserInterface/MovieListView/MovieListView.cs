﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MovieTracker.Movies;
using System.Diagnostics;

namespace MovieTracker.UserInterface
{
    /// <summary>
    /// MovieListView class.
    /// </summary>
    public partial class MovieListView : ListView
    {
        // Responsable for managing the movie collection.
        private MovieCollection movieCollection = null;

        // The sorting order of the Groups.
        private SortOrder groupSortOrder = SortOrder.Ascending;
        
        #region Properties

        /// <summary>
        /// Gets or sets the sort order of the the groups in this control.
        /// </summary>
        public SortOrder GroupSortOrder
        {
            get { return this.groupSortOrder; }
            set
            {
                this.groupSortOrder = value;

                // Perform sorting operation on the Groups
                this.SortGroups(this.groupSortOrder);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public MovieListView()
        {
            InitializeComponent();
                        
            // Setup LargeImageList, where the thumbnails are stored.
            this.LargeImageList = new ImageList();
            this.LargeImageList.ImageSize = new Size(110, 150);
            this.LargeImageList.ColorDepth = ColorDepth.Depth32Bit;
            
            // Set this style will reduce flicker.                        
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            
            // Set the items height in Details view mode.
            this.SetItemHeight(24);

            // Order the items in ascending order.
            this.Sorting = SortOrder.Ascending;

        }

        #endregion 

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        public void bind(MovieCollection movieCollection)
        {
            this.movieCollection = movieCollection;
        }

        /// <summary>
        /// Adds a movie to the ListView control.
        /// </summary>
        public void AddMovie(Movie movie)
        {
            
            // Add thumbnail to LargeImageList
            LargeImageList.Images.Add(movie.Id.ToString(), movie.Thumbnail);
                  
                        
            ListViewItem item = this.Items.Add(movie.Id.ToString(), movie.Title, movie.Id.ToString());
            
            item.Tag = movie.Id;    // Store Id of the movie in the Tag field.
           
            item.SubItems.Add(movie.Genre);             // Genre
            item.SubItems.Add(movie.Director);          // Director
            item.SubItems.Add(movie.Year.ToString());   // Year
                        
            // The movies are grouped by year, so we have to verify if the group
            // exists and if not then we have to create it.
            if (!isGroup(movie.Year.ToString()))
            {
                this.Groups.Add(movie.Year.ToString(), movie.Year.ToString());

                // Only sort the groups if a new one was added.
                this.SortGroups(GroupSortOrder);
            }

            // Set the group of the current movie/item.
            item.Group = this.Groups[movie.Year.ToString()];

            this.EnsureVisible(item.Index);
            this.Sort();
        }
        
        /// <summary>
        /// Update information of a movie in the ListView.
        /// </summary>
        public void UpdateMovie(Movie movie)
        {
            ListViewItem item = this.Items[movie.Id.ToString()];

            item.Text = movie.Title;                        // Title
            item.SubItems[1].Text = movie.Genre;            // Genre
            item.SubItems[2].Text = movie.Director;         // Director
            item.SubItems[3].Text = movie.Year.ToString();  // Year

            // Replace the thumbanil of the movie.
            this.ReplaceThumbnail(movie);

            // The movies are grouped by year, so we have to verify if the group
            // was not already created and if not then we have to create it.
            if (!isGroup(movie.Year.ToString()))
            {
                this.Groups.Add(movie.Year.ToString(), movie.Year.ToString());

                // Only sort the groups if a new one was added.
                this.SortGroups(this.GroupSortOrder);
            }

            // Set the group of the current movie/item.
            item.Group = this.Groups[movie.Year.ToString()];

            this.EnsureVisible(item.Index);
            this.Sort();
        }

        /// <summary>
        /// Remove a movie from the ListView.
        /// </summary>
        public void RemoveMovie(Movie movie)
        {
            // Remove the item from the ListView.
            this.Items[movie.Id.ToString()].Remove();
                       
            // Remove the thumbnail from the ImageList.
            this.LargeImageList.Images.RemoveByKey(movie.Id.ToString());
        }

        /// <summary>
        /// Return the movie that was selected in the ListView
        /// <summary>
        public Movie GetSelectedMovie()
        {
            if (SelectedItems.Count == 1)
            {
                return this.movieCollection.GetMovie((int)SelectedItems[0].Tag);
            }

            return null;
        }

        /// <summary>
        /// Select the specified item and make soure it is visible.
        /// </summary>
        public void SelectMovie(Movie movie)
        {
            // Scroll to the specified item.
            this.Items[movie.Id.ToString()].EnsureVisible();

            // Select the specified item.
            this.Items[movie.Id.ToString()].Selected = true;
        }
        
        /// <summary>
        /// Load the list of movies into the ListView.
        /// </summary>
        public int LoadMovies()
        {
            List<Movie> movieList = movieCollection.GetMovies();
            
            // Remove previous items form the ListView.
            this.LargeImageList.Images.Clear();
            
            this.Items.Clear();
            this.BeginUpdate();

            // Add each movie from the collection to the ListView.
            foreach (var movie in movieList)
            {
                this.AddMovie(movie);
            }

            this.EndUpdate();

            return movieList.Count;
        }

        /// <summary>
        /// Search for the movies that match the specified criteries and load them into the ListView.
        /// </summary>
        public int SearchMovies(String title, String genre, String director, int year)
        {
            List<Movie> movieList = movieCollection.SearchMovies(title, genre, director, year);

            // Remove previous items form the ListView.
            Items.Clear();
            LargeImageList.Images.Clear();

            BeginUpdate();

            // Add each movie from the collection to the ListView.
            foreach (var movie in movieList)
            {
                AddMovie(movie);
            }

            EndUpdate();

            return movieList.Count;
        }

        /// <summary>
        /// Returns true there already exists a group with the specified name.
        /// </summary>
        private bool isGroup(String name)
        {
            // Go through all the groups and verify there names.
            foreach (ListViewGroup group in this.Groups)
            {
                if (group.Name.Equals(name))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Sort the groups of the ListView control.
        /// </summary>
        private void SortGroups(SortOrder order)
        {
            List<ListViewGroup> iList = new List<ListViewGroup>();

            // Perfrom the sorting only if the items are displayed in groups by the ListView,
            // and there is more then one group.
            if (this.ShowGroups && (this.Groups.Count > 1))
            {
                // Prevent the ListView from drawing.
                this.BeginUpdate();

                // Copy all the groups form the ListView to a List.
                foreach (ListViewGroup Group in this.Groups)
                {
                    iList.Add(Group);
                }

                // Perform the sorting of the List.
                iList.Sort(new ListViewGroupSorter(order));

                // Copy back the groups in sorted order.
                this.Groups.Clear();
                this.Groups.AddRange(iList.ToArray());

                // Resume drawing of the list view control.
                this.EndUpdate();
            }

            this.Sort();
        }

        /// <summary>
        /// Set the items height in Details view mode.
        /// </summary>
        private void SetItemHeight(int height)
        {
            ImageList imgList = new ImageList();
            imgList.ImageSize = new Size(1, height);

            this.SmallImageList = imgList;
        }
        
        /// <summary>
        /// Replace the movie poster in the LargeImageList.
        /// </summary>
        private void ReplaceThumbnail(Movie movie)
        {
           
            LargeImageList.Images.RemoveByKey(movie.Id.ToString());
            LargeImageList.Images.Add(movie.Id.ToString(), movie.Thumbnail);

            Refresh();

            // NOTE:
            //  Apperantly there is a bug in the ListView Control. When is Details view mode the
            //  the images will disapear if they are replaced.

            FixIt();
        }

        /// <summary>
        /// There is a bug in the ListView component when swiching to LargeIcon viewmode,
        /// the images will not be loaded. So this function needs to be called after swiching the ViewMode.
        /// </summary>
        public void FixIt()
        {
            // Trigger a reload of the ListView.LargeImageList
            if (this.View == View.LargeIcon)
            {
                var key = "Dummy image to be deleted right after its insertion...";
                
                this.LargeImageList.Images.Add(key, new Bitmap(1, 1));
                this.LargeImageList.Images.RemoveByKey(key);
            }
        }
        #endregion
    }

}
