﻿namespace MovieTracker.UserInterface
{
    partial class MoviePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tboxCast = new System.Windows.Forms.TextBox();
            this.tboxDescription = new System.Windows.Forms.TextBox();
            this.tboxId = new System.Windows.Forms.TextBox();
            this.tboxYear = new System.Windows.Forms.TextBox();
            this.tboxDirector = new System.Windows.Forms.TextBox();
            this.tboxGenre = new System.Windows.Forms.TextBox();
            this.tboxTitle = new System.Windows.Forms.TextBox();
            this.btnPoster = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.cboxType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudRating = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRating)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tboxCast);
            this.groupBox1.Controls.Add(this.tboxDescription);
            this.groupBox1.Controls.Add(this.tboxId);
            this.groupBox1.Controls.Add(this.tboxYear);
            this.groupBox1.Controls.Add(this.tboxDirector);
            this.groupBox1.Controls.Add(this.tboxGenre);
            this.groupBox1.Controls.Add(this.tboxTitle);
            this.groupBox1.Controls.Add(this.btnPoster);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.cboxType);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nudRating);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(4, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(521, 601);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // tboxCast
            // 
            this.tboxCast.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxCast.Location = new System.Drawing.Point(209, 194);
            this.tboxCast.Multiline = true;
            this.tboxCast.Name = "tboxCast";
            this.tboxCast.Size = new System.Drawing.Size(279, 20);
            this.tboxCast.TabIndex = 89;
            this.tboxCast.WordWrap = false;
            // 
            // tboxDescription
            // 
            this.tboxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxDescription.Location = new System.Drawing.Point(102, 316);
            this.tboxDescription.Multiline = true;
            this.tboxDescription.Name = "tboxDescription";
            this.tboxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxDescription.Size = new System.Drawing.Size(386, 125);
            this.tboxDescription.TabIndex = 88;
            // 
            // tboxId
            // 
            this.tboxId.Enabled = false;
            this.tboxId.Location = new System.Drawing.Point(64, 36);
            this.tboxId.Name = "tboxId";
            this.tboxId.Size = new System.Drawing.Size(66, 20);
            this.tboxId.TabIndex = 86;
            // 
            // tboxYear
            // 
            this.tboxYear.Location = new System.Drawing.Point(212, 262);
            this.tboxYear.Name = "tboxYear";
            this.tboxYear.Size = new System.Drawing.Size(86, 20);
            this.tboxYear.TabIndex = 80;
            // 
            // tboxDirector
            // 
            this.tboxDirector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxDirector.Location = new System.Drawing.Point(209, 131);
            this.tboxDirector.Name = "tboxDirector";
            this.tboxDirector.Size = new System.Drawing.Size(279, 20);
            this.tboxDirector.TabIndex = 77;
            // 
            // tboxGenre
            // 
            this.tboxGenre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxGenre.Location = new System.Drawing.Point(209, 161);
            this.tboxGenre.Name = "tboxGenre";
            this.tboxGenre.Size = new System.Drawing.Size(279, 20);
            this.tboxGenre.TabIndex = 76;
            // 
            // tboxTitle
            // 
            this.tboxTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxTitle.Location = new System.Drawing.Point(61, 71);
            this.tboxTitle.Name = "tboxTitle";
            this.tboxTitle.Size = new System.Drawing.Size(326, 20);
            this.tboxTitle.TabIndex = 71;
            this.tboxTitle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tboxTitle_KeyDown);
            // 
            // btnPoster
            // 
            this.btnPoster.Location = new System.Drawing.Point(19, 116);
            this.btnPoster.Name = "btnPoster";
            this.btnPoster.Size = new System.Drawing.Size(125, 175);
            this.btnPoster.TabIndex = 87;
            this.btnPoster.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(19, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 85;
            this.label15.Text = "Id:";
            // 
            // cboxType
            // 
            this.cboxType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxType.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboxType.FormatString = "  ";
            this.cboxType.FormattingEnabled = true;
            this.cboxType.Items.AddRange(new object[] {
            "Movie",
            "Documentary",
            "Mini-Series"});
            this.cboxType.Location = new System.Drawing.Point(209, 33);
            this.cboxType.Name = "cboxType";
            this.cboxType.Size = new System.Drawing.Size(216, 21);
            this.cboxType.TabIndex = 84;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(162, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 83;
            this.label9.Text = "Type:";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(150, 200);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 82;
            this.label14.Text = "Cast:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 316);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 81;
            this.label10.Text = "Description:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(150, 265);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 79;
            this.label1.Text = "Year:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudRating
            // 
            this.nudRating.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudRating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudRating.DecimalPlaces = 1;
            this.nudRating.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudRating.Location = new System.Drawing.Point(209, 226);
            this.nudRating.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudRating.Name = "nudRating";
            this.nudRating.Size = new System.Drawing.Size(161, 20);
            this.nudRating.TabIndex = 78;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(150, 167);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 75;
            this.label12.Text = "Genre:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(150, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 74;
            this.label8.Text = "Rating:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(149, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 73;
            this.label4.Text = "Directors:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(404, 69);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(84, 23);
            this.btnSearch.TabIndex = 72;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Title:";
            // 
            // MoviePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "MoviePanel";
            this.Size = new System.Drawing.Size(529, 604);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRating)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tboxCast;
        private System.Windows.Forms.TextBox tboxDescription;
        private System.Windows.Forms.TextBox tboxId;
        private System.Windows.Forms.TextBox tboxYear;
        private System.Windows.Forms.TextBox tboxDirector;
        private System.Windows.Forms.TextBox tboxGenre;
        private System.Windows.Forms.TextBox tboxTitle;
        private System.Windows.Forms.Button btnPoster;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboxType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudRating;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
    }
}
