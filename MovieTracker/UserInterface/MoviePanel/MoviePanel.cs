﻿using System;
using System.Windows.Forms;
using MovieTracker.NetFetch;
using MovieTracker.Movies;
using System.Drawing;
using MovieTracker.Utils;
using System.Threading;

namespace MovieTracker.UserInterface
{
    /// <summary>
    /// 
    /// </summary>
    public partial class MoviePanel : UserControl
    {
        #region Members

        // Form which displays list of movies found after performing a search on website.
        private FormSearchResult formSearchResult = new FormSearchResult();

        // Form that is displayed while the user of slow operation.
        private FormWaitResult formWaitResult = new FormWaitResult();

        // The movie that about which information is displayed in this Control.
        private Movie movie = null;

        // Responsable for managing the NetFetchers(aka Plugins).
        private NetFetchManager netFetchManager = null;

        // Responsable for managing the collection of movies.
        private MovieCollection movieCollection = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public MoviePanel()
        {
            InitializeComponent();
        }

        #endregion

        #region Injector

        /// <summary>
        /// 
        /// </summary>
        public void bind(MovieCollection movieCollection, NetFetchManager netFetchManager)
        {
            this.movieCollection = movieCollection;
            this.netFetchManager = netFetchManager;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Movie Movie
        {
            get
            {
                return this.movie;
            } 

            set
            {
                this.movie = value;
                                                
                if (this.Movie != null)
                {
                    this.MovieToPanel();
                }
                else
                {
                    this.ClearPanel();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Id
        {
            get
            {
                return int.Parse(this.tboxId.Text);
            }

            private set
            {
                if (value == -1)
                {
                    this.tboxId.Text = String.Empty;
                }
                else
                {
                    this.tboxId.Text = value.ToString();
                }
            }
        }

        /// <summary>
        /// Gets the String present in the Title field.
        /// </summary>
        public String Title
        {
            get
            {
                return this.tboxTitle.Text.Trim();
            }

            private set
            {
                this.tboxTitle.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Type
        {
            get
            {
                return this.cboxType.SelectedIndex;
            }

            private set
            {
                this.cboxType.SelectedIndex = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Director
        {
            get
            {
                return this.tboxDirector.Text.Trim();
            }

            private set
            {
                this.tboxDirector.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Genre
        {
            get
            {

                return this.tboxGenre.Text.Trim();
            }

            private set
            {

                this.tboxGenre.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Cast
        {
            get
            {
                return this.tboxCast.Text.Trim();
            }

            private set
            {

                this.tboxCast.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Description
        {
            get
            {

                return this.tboxDescription.Text.Trim();
            }

            private  set
            {

                this.tboxDescription.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Year
        {
            get
            {
                return this.tboxYear.Text.Trim();
            }

            private set
            {
                this.tboxYear.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Rating
        {
            get
            {
                return this.nudRating.Value;
            }

            private set
            {
                this.nudRating.Value = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Url
        {
            get; private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Image Poster
        {
            get
            {
                return this.btnPoster.Tag as Image;
            }

            private set
            {
                this.btnPoster.Tag = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Image Thumbnail
        {
            get
            {
                return this.btnPoster.Image;
            }

            private set
            {
                this.btnPoster.Image = value;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the "Search" button`s click event.
        /// </summary>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.SearchForMovie();
        }

        /// <summary>
        /// Handles the KeyDown event of TextBox containing the Title.
        /// </summary>
        private void tboxTitle_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SearchForMovie();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clear all the fields of the Control.
        /// </summary>
        public void ClearPanel()
        {
            this.Id     = -1;
            this.Title  = String.Empty;
            this.Type   = -1;
            this.Director   = String.Empty;
            this.Genre  = String.Empty;
            this.Cast   = String.Empty;
            this.Rating = new decimal(0);
            this.Description = String.Empty;
            this.Url    =  String.Empty;
            this.Poster     = null;
            this.Thumbnail  = null;
        }

        /// <summary>
        /// Fill the Control`s fields with information from the NetFetchManager.
        /// </summary>
        private void NetFetchToPanel()
        {
            this.Title  = netFetchManager.Title;
            this.Type   = -1;
            this.Director = netFetchManager.Director;
            this.Genre  = netFetchManager.Genre;
            this.Cast   = netFetchManager.Cast;
            this.Rating = netFetchManager.Rating;
            this.Year   = netFetchManager.Year;
            this.Description = netFetchManager.Description;
            this.Url    = netFetchManager.Url;
            this.Poster = netFetchManager.Poster;

            // Generate the Thumbnail from the Poster of the Movie.
            this.Thumbnail = Utility.CreateThumbnail(Poster, 110, 150, 3);
        }

        /// <summary>
        /// Fill the Control`s fields with information from the Movie property.
        /// </summary>
        private void MovieToPanel()
        {
            this.Id     = Movie.Id;
            this.Title  = Movie.Title;
            this.Type   = Movie.Type;
            this.Director = Movie.Director;
            this.Genre  = Movie.Genre;
            this.Cast   = Movie.Cast;
            this.Rating = Movie.Rating;
            this.Year   = Movie.Year.ToString();
            this.Description = Movie.Description;
            this.Url    = Movie.URL;
            this.Poster = Movie.Poster;

            // Generate the Thumbnail from the Poster of the Movie.
            this.Thumbnail = Movie.Thumbnail;
        }

        /// <summary>
        /// Search for the movie thas has the title specified the by user and fetch information 
        /// from the website if necessary.
        /// </summary>
        private void SearchForMovie()
        {
            // Verify the title. A title must be specified before performing the search.
            if (this.Title == String.Empty)
            {
                Utility.ShowMessage("Please enter the movie title before performing search!");

                return;
            }

            // Search for the title specified.
            this.PerformSearch(this.Title);

            // Verify the number of results.
            if (netFetchManager.Results.Count == 0)
            {
                Utility.ShowMessage("Sorry, no movie was find the specfied title!");

                return;
            }

            // Display dialog so the user can select form the list of results.
            if (formSearchResult.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            // Parse the selected movie`s website.
            this.PerformFetch(formSearchResult.URL);
        }

        /// <summary>
        /// Search for the movie thas has the title specified the by user.
        /// </summary>
        public void PerformSearch(String title)
        {
            // Execute the following operation on another thread:
            //  Perform the search on the website for the title introduced by the user.
            new Thread(() => _SearchMovie(title)).Start();


            // Display a form while background operation is performed.
            formWaitResult.ShowDialog();
        }

        /// <summary>
        /// Search operation that will be executed on a separate thread.
        /// </summary>
        private void _SearchMovie(String title)
        {
            netFetchManager.Search(title);

            // Load the search result into a another Form.
            if (formSearchResult.InvokeRequired)
            {
                formSearchResult.BeginInvoke(new MethodInvoker(() => this.LoadSearchResult()));
            }
            else
            {
                this.LoadSearchResult();
            }

            // Hide the Wait Form.
            formWaitResult.Invoke(new MethodInvoker(() => formWaitResult.Hide()));
        }

        /// <summary>
        /// Search information for the movie selected by the user.
        /// </summary>
        private void PerformFetch(String url)
        {
            // Execute the following operation on another thread:
            //  Fetch information from website about the specified movie.
            new Thread(() => _FetchMovie(url)).Start();


            // Display a form while background operation is performed.
            formWaitResult.ShowDialog();
        }

        /// <summary>
        /// Fetch operation that is executed on a separate thread.
        /// </summary>
        private void _FetchMovie(String url)
        {
            netFetchManager.Fetch(url);

            // Load the fetch result into the Form.
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => this.NetFetchToPanel()));
            }
            else
            {
                this.NetFetchToPanel();
            }

            // Hide the Wait Form.
            formWaitResult.Invoke(new MethodInvoker(() => formWaitResult.Hide()));
        }

        /// <summary>
        /// Load the search result of the NetFetchManager into the ResultForm.
        /// </summary>
        private void LoadSearchResult()
        {
            formSearchResult.Clear();   // Remove previous search result from the dialog.

            foreach (var result in this.netFetchManager.Results)
            {
                formSearchResult.AddResult(result.Title, result.Description, result.Url);
            }
        }

        #endregion
  
    }
}
