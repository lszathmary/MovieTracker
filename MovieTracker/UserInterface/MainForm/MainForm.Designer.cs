﻿namespace MovieTracker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnWebPage = new System.Windows.Forms.Button();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thumbnailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.bgWorkerLoad = new System.ComponentModel.BackgroundWorker();
            this.bgWorkerNetFetch = new System.ComponentModel.BackgroundWorker();
            this.tbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tbtnRemove = new System.Windows.Forms.ToolStripButton();
            this.tbtnSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnViewMode = new System.Windows.Forms.ToolStripSplitButton();
            this.menuDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.menuThumbnails = new System.Windows.Forms.ToolStripMenuItem();
            this.lblWebSite = new System.Windows.Forms.ToolStripLabel();
            this.cboxWebSite = new System.Windows.Forms.ToolStripComboBox();
            this.lblGrouped = new System.Windows.Forms.ToolStripLabel();
            this.cboxGrouped = new System.Windows.Forms.ToolStripComboBox();
            this.btnAbout = new System.Windows.Forms.ToolStripButton();
            this.lblOrder = new System.Windows.Forms.ToolStripLabel();
            this.cboxOrder = new System.Windows.Forms.ToolStripComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.listView = new MovieTracker.UserInterface.MovieListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.moviePanel = new MovieTracker.UserInterface.MoviePanel();
            this.statusStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblCount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 703);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1216, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblCount
            // 
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(0, 17);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1216, 661);
            this.splitContainer1.SplitterDistance = 699;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 3;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer_SplitterMoved);
            this.splitContainer1.DoubleClick += new System.EventHandler(this.splitContainer1_DoubleClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.moviePanel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(509, 661);
            this.panel1.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.btnSave, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnWebPage, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 610);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(509, 51);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Location = new System.Drawing.Point(331, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnWebPage
            // 
            this.btnWebPage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnWebPage.Location = new System.Drawing.Point(77, 6);
            this.btnWebPage.Name = "btnWebPage";
            this.btnWebPage.Size = new System.Drawing.Size(100, 23);
            this.btnWebPage.TabIndex = 1;
            this.btnWebPage.Text = "Web Page";
            this.btnWebPage.UseVisualStyleBackColor = true;
            this.btnWebPage.Click += new System.EventHandler(this.btnWebPage_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Title";
            this.columnHeader1.Width = 170;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Genre";
            this.columnHeader2.Width = 125;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Director";
            this.columnHeader3.Width = 126;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Rating";
            this.columnHeader4.Width = 70;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Year";
            // 
            // detailsToolStripMenuItem
            // 
            this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
            this.detailsToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.detailsToolStripMenuItem.Text = "Details";
            // 
            // tileToolStripMenuItem
            // 
            this.tileToolStripMenuItem.Name = "tileToolStripMenuItem";
            this.tileToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.tileToolStripMenuItem.Text = "Tile";
            // 
            // thumbnailToolStripMenuItem
            // 
            this.thumbnailToolStripMenuItem.Name = "thumbnailToolStripMenuItem";
            this.thumbnailToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.thumbnailToolStripMenuItem.Text = "Thumbnail";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.CheckOnClick = true;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.CheckOnClick = true;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "toolStripMenuItem2";
            // 
            // tbtnAdd
            // 
            this.tbtnAdd.AutoSize = false;
            this.tbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAdd.Image")));
            this.tbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAdd.Name = "tbtnAdd";
            this.tbtnAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tbtnAdd.Size = new System.Drawing.Size(90, 40);
            this.tbtnAdd.Text = "Add";
            this.tbtnAdd.ToolTipText = "Add";
            this.tbtnAdd.Click += new System.EventHandler(this.tbtnAdd_Click);
            // 
            // tbtnRemove
            // 
            this.tbtnRemove.AutoSize = false;
            this.tbtnRemove.Image = ((System.Drawing.Image)(resources.GetObject("tbtnRemove.Image")));
            this.tbtnRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnRemove.Name = "tbtnRemove";
            this.tbtnRemove.Size = new System.Drawing.Size(90, 40);
            this.tbtnRemove.Text = "Remove";
            this.tbtnRemove.Click += new System.EventHandler(this.tbtnRemove_Click);
            // 
            // tbtnSearch
            // 
            this.tbtnSearch.AutoSize = false;
            this.tbtnSearch.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSearch.Image")));
            this.tbtnSearch.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSearch.Name = "tbtnSearch";
            this.tbtnSearch.Size = new System.Drawing.Size(90, 40);
            this.tbtnSearch.Text = "Search";
            this.tbtnSearch.Click += new System.EventHandler(this.tbtnSearch_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 42);
            // 
            // tbtnViewMode
            // 
            this.tbtnViewMode.AutoSize = false;
            this.tbtnViewMode.DropDownButtonWidth = 12;
            this.tbtnViewMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuDetails,
            this.menuThumbnails});
            this.tbtnViewMode.Image = ((System.Drawing.Image)(resources.GetObject("tbtnViewMode.Image")));
            this.tbtnViewMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnViewMode.Name = "tbtnViewMode";
            this.tbtnViewMode.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tbtnViewMode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbtnViewMode.RightToLeftAutoMirrorImage = true;
            this.tbtnViewMode.Size = new System.Drawing.Size(90, 35);
            this.tbtnViewMode.Text = "View";
            this.tbtnViewMode.ToolTipText = "View";
            // 
            // menuDetails
            // 
            this.menuDetails.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.menuDetails.AutoSize = false;
            this.menuDetails.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menuDetails.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuDetails.MergeAction = System.Windows.Forms.MergeAction.Remove;
            this.menuDetails.Name = "menuDetails";
            this.menuDetails.Padding = new System.Windows.Forms.Padding(0);
            this.menuDetails.Size = new System.Drawing.Size(170, 25);
            this.menuDetails.Text = "  Details";
            this.menuDetails.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.menuDetails.Click += new System.EventHandler(this.detailsMenuItem_Click);
            // 
            // menuThumbnails
            // 
            this.menuThumbnails.Checked = true;
            this.menuThumbnails.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menuThumbnails.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuThumbnails.Name = "menuThumbnails";
            this.menuThumbnails.Size = new System.Drawing.Size(143, 22);
            this.menuThumbnails.Text = "  Thumbnails";
            this.menuThumbnails.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.menuThumbnails.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.menuThumbnails.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.menuThumbnails.Click += new System.EventHandler(this.thumbnailsMenuItem_Click);
            // 
            // lblWebSite
            // 
            this.lblWebSite.AutoSize = false;
            this.lblWebSite.BackColor = System.Drawing.SystemColors.Control;
            this.lblWebSite.Name = "lblWebSite";
            this.lblWebSite.Size = new System.Drawing.Size(70, 35);
            this.lblWebSite.Text = "Source:";
            // 
            // cboxWebSite
            // 
            this.cboxWebSite.AutoSize = false;
            this.cboxWebSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxWebSite.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboxWebSite.Name = "cboxWebSite";
            this.cboxWebSite.Size = new System.Drawing.Size(90, 23);
            this.cboxWebSite.SelectedIndexChanged += new System.EventHandler(this.cboxWebSite_SelectedIndexChanged);
            // 
            // lblGrouped
            // 
            this.lblGrouped.AutoSize = false;
            this.lblGrouped.Name = "lblGrouped";
            this.lblGrouped.Size = new System.Drawing.Size(70, 40);
            this.lblGrouped.Text = "Group:";
            // 
            // cboxGrouped
            // 
            this.cboxGrouped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxGrouped.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cboxGrouped.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cboxGrouped.Name = "cboxGrouped";
            this.cboxGrouped.Size = new System.Drawing.Size(90, 42);
            this.cboxGrouped.SelectedIndexChanged += new System.EventHandler(this.cboxGrouped_SelectedIndexChanged);
            // 
            // btnAbout
            // 
            this.btnAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnAbout.AutoSize = false;
            this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
            this.btnAbout.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(100, 35);
            this.btnAbout.Text = "About";
            this.btnAbout.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAbout.Visible = false;
            this.btnAbout.Click += new System.EventHandler(this.tbtnAbout_Click);
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = false;
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(70, 40);
            this.lblOrder.Text = "Order:";
            // 
            // cboxOrder
            // 
            this.cboxOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxOrder.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboxOrder.Items.AddRange(new object[] {
            "Ascending",
            "Descending"});
            this.cboxOrder.Name = "cboxOrder";
            this.cboxOrder.Size = new System.Drawing.Size(90, 42);
            this.cboxOrder.SelectedIndexChanged += new System.EventHandler(this.cboxOrder_SelectedIndexChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnAdd,
            this.tbtnRemove,
            this.tbtnSearch,
            this.toolStripSeparator1,
            this.lblWebSite,
            this.cboxWebSite,
            this.lblGrouped,
            this.cboxGrouped,
            this.btnAbout,
            this.lblOrder,
            this.cboxOrder,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.tbtnViewMode});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStrip1.Size = new System.Drawing.Size(1216, 42);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(16, 39);
            this.toolStripLabel1.Text = "   ";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 42);
            // 
            // listView
            // 
            this.listView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader10});
            this.listView.FullRowSelect = true;
            this.listView.GroupSortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listView.Location = new System.Drawing.Point(12, 3);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.ShowGroups = false;
            this.listView.Size = new System.Drawing.Size(684, 644);
            this.listView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.ListView_SelectedIndexChanged);
            this.listView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listView_KeyDown);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Title";
            this.columnHeader6.Width = 180;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Genre";
            this.columnHeader7.Width = 170;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Director";
            this.columnHeader8.Width = 230;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Year";
            this.columnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // moviePanel
            // 
            this.moviePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.moviePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.moviePanel.Location = new System.Drawing.Point(4, 0);
            this.moviePanel.Movie = null;
            this.moviePanel.Name = "moviePanel";
            this.moviePanel.Size = new System.Drawing.Size(501, 604);
            this.moviePanel.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 725);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MovieTracker";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private UserInterface.MovieListView listView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thumbnailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.ComponentModel.BackgroundWorker bgWorkerLoad;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        private System.ComponentModel.BackgroundWorker bgWorkerNetFetch;
        private UserInterface.MoviePanel moviePanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnWebPage;
        private System.Windows.Forms.ToolStripButton tbtnAdd;
        private System.Windows.Forms.ToolStripButton tbtnRemove;
        private System.Windows.Forms.ToolStripButton tbtnSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSplitButton tbtnViewMode;
        private System.Windows.Forms.ToolStripMenuItem menuDetails;
        private System.Windows.Forms.ToolStripMenuItem menuThumbnails;
        private System.Windows.Forms.ToolStripLabel lblWebSite;
        private System.Windows.Forms.ToolStripComboBox cboxWebSite;
        private System.Windows.Forms.ToolStripLabel lblGrouped;
        private System.Windows.Forms.ToolStripComboBox cboxGrouped;
        private System.Windows.Forms.ToolStripButton btnAbout;
        private System.Windows.Forms.ToolStripLabel lblOrder;
        private System.Windows.Forms.ToolStripComboBox cboxOrder;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
    }
}

