﻿using System;
using System.Windows.Forms;
using MovieTracker.Movies;
using MovieTracker.NetFetch;
using MovieTracker.UserInterface;
using System.Threading;
using MovieTracker.Utils;
using System.Diagnostics;

namespace MovieTracker
{
    /// <summary>
    /// MainForm class.
    ///     Represents the main window of the application.
    /// </summary>
    public partial class MainForm : Form
    {
        // Responsable for managing the movie collection.
        private MovieCollection movieCollection = new MovieCollection();

        // Responsable for managing the NetFetchers (aka plugins).
        private NetFetchManager netFetchManager = new NetFetchManager();
        
        // Form that is displayed while the user has to wait becasue of slow operation.
        private FormWaitResult frmWaitResult = new FormWaitResult();

        // Form which displays list of movies found after performing a search on website for a movie title.
        private FormSearchResult frmSearchResult = new FormSearchResult();

        // Form used for adding a new movie to the collection.
        private FormAddMovie frmAddMovie = new FormAddMovie();

        // Form used for search for a movie in the collection.
        private FormSearchMovie frmSearchMovie = new FormSearchMovie();

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
               

            this.frmAddMovie.bind(this.movieCollection, this.netFetchManager);
            this.moviePanel.bind(this.movieCollection, this.netFetchManager);
            this.listView.bind(this.movieCollection);
            this.frmSearchMovie.bind(listView);
                        
            // Fill the combobox with the name of the NetFetchers.
            this.cboxWebSite.Items.AddRange(netFetchManager.GetNetFetchers());
            this.cboxWebSite.SelectedIndex = 0;

            // Items are grouped in the ListView.
            this.cboxGrouped.SelectedIndex = 0;
            this.listView.ShowGroups = true;

            // Groups are ordered in ascending order.
            this.cboxOrder.SelectedIndex = 0;
            this.listView.GroupSortOrder = SortOrder.Ascending;

            // Non of the movies is selected.
            this.moviePanel.Enabled = false;

            // Perform backgorund operation:
            //    Load the list of movies present in the collection.
            new Thread(DoLoadMovies).Start();
            
            // Display a form while background operation is performed.
            this.frmWaitResult.ShowDialog("Loading...");
        }

        #endregion 

        /// <summary>
        /// Handles the "Add" button`s click event.
        /// </summary>
        private void tbtnAdd_Click(object sender, EventArgs e)
        {
            this.AddMovie();
        }
        
        /// <summary>
        /// Handles the "Remove" button`s click event.
        /// </summary>
        private void tbtnRemove_Click(object sender, EventArgs e)
        {
            this.RemoveMovie();          
        }

        /// <summary>
        /// Handles the "Search" button`s click event.
        /// </summary>
        private void tbtnSearch_Click(object sender, EventArgs e)
        {
            this.SearchMovie();
        }

        /// <summary>
        /// Handles the "About" button`s click event.
        /// </summary>
        private void tbtnAbout_Click(object sender, EventArgs e)
        {
            FormCSVImport frmCVSImport = new FormCSVImport();

            frmCVSImport.StartPosition = FormStartPosition.CenterParent;
            frmCVSImport.bind(movieCollection, listView);
            frmCVSImport.ShowDialog();
           
        }

        /// <summary>
        /// Handles the "Details" menu items`s click event.
        /// </summary>
        private void detailsMenuItem_Click(object sender, EventArgs e)
        {
            if (listView.View != View.Details)
            {
                // Change view mode of the ListView Details.
                listView.View = View.Details;

                this.menuDetails.Checked = true;
                this.menuThumbnails.Checked = false;
            }
        }

        /// <summary>
        /// Handles the "Thumbnails" menu items`s click event.
        /// </summary>
        private void thumbnailsMenuItem_Click(object sender, EventArgs e)
        {
            if (this.listView.View != View.LargeIcon)
            {
                // Change view mode of the ListView to LargIcon.
                this.listView.View = View.LargeIcon;

                this.menuDetails.Checked = false;
                this.menuThumbnails.Checked = true;

                listView.FixIt();
            }
        }

        /// <summary>
        /// Handles the "Website" combobox`s selected index changed event.
        /// </summary>
        private void cboxWebSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Change the active NetFetcher.
            this.netFetchManager.SetActiveNetFetcher(this.cboxWebSite.SelectedItem.ToString());
        }

        /// <summary>
        /// Handles the "Grouped" combobox`s selected index changed event.
        /// </summary>
        private void cboxGrouped_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView.BeginUpdate();

            // Group Items in ListView == Yes
            if (cboxGrouped.SelectedIndex == 0)
            {
                listView.ShowGroups = true;
            }

            // Group Items in ListView == No
            if (cboxGrouped.SelectedIndex == 1)
            {
                listView.ShowGroups = false;
            }

            listView.Sort();
            listView.EndUpdate();
        }

        /// <summary>
        /// Handles the "Group Order" combobox`s selected index changed event.
        /// </summary>
        private void cboxOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            // GroupOrder in the ListView => Ascending
            if (cboxOrder.Items[cboxOrder.SelectedIndex].ToString() == "Ascending")
            {
                listView.GroupSortOrder = SortOrder.Ascending;
            }

            // Group Order in the ListView => Descending
            if (cboxOrder.Items[cboxOrder.SelectedIndex].ToString() == "Descending")
            {
                listView.GroupSortOrder = SortOrder.Descending;
            }
        }
         
        /// <summary>
        /// 
        /// </summary>
        private void ListView_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
            {
                this.moviePanel.Enabled = false;
                this.moviePanel.Movie   = null;
            }

            if (listView.SelectedItems.Count == 1)
            {
                this.moviePanel.Enabled = true;
                this.moviePanel.Movie   = this.listView.GetSelectedMovie();
            }
        }
           
        /// <summary>
        /// 
        /// </summary>
        private void splitContainer_SplitterMoved(object sender, SplitterEventArgs e)
        {
            // NOTE:
            //  There is a bug in SplitContainer control, it remains selected after the user 
            //  stopped dragging it, so in order to avoid that we select the ListView.
            listView.Select();
        }
        
       

        /// <summary>
        /// 
        /// </summary>
        private void listView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                this.RemoveMovie();
            }

            if ((e.KeyCode == Keys.F) && (e.Control))
            {
                this.SearchMovie();
            }

            if ((e.KeyCode == Keys.A) && (e.Control))
            {
                this.AddMovie();
            }

        }

        /// <summary>
        /// Handles the "WebPage" button`s click event.
        /// </summary>
        private void btnWebPage_Click(object sender, EventArgs e)
        {
            if ((this.moviePanel.Url == null) || (this.moviePanel.Url == String.Empty)) return;

            Process process = new Process();

            try
            {
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.FileName = this.moviePanel.Url;
                process.Start();
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Handles the "Save" button`s click event.
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.moviePanel.Type == -1)
            {
                MessageBox.Show("Please select the type of the movie!");

                return;
            }

            listView.BeginUpdate();

            var id = (this.moviePanel.Id);
            
            // Change movie information in the collection.
            this.movieCollection.UpdateMovie(id, this.moviePanel.Title,
                                        this.moviePanel.Type,
                                        this.moviePanel.Genre,
                                        this.moviePanel.Director,
                                        this.moviePanel.Cast,
                                        this.moviePanel.Description,
                                        this.moviePanel.Rating,
                                        Convert.StringToInt(this.moviePanel.Year),
                                        this.moviePanel.Url,
                                        this.moviePanel.Poster);
                       
            // Change movie information in the ListView item.
            this.listView.UpdateMovie(movieCollection.GetMovie(this.moviePanel.Id));

            listView.EndUpdate();
        }

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        private void AddMovie()
        {
            if (this.frmAddMovie.ShowDialog() == DialogResult.OK)
            {
                this.listView.AddMovie(frmAddMovie.Movie);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void RemoveMovie()
        {
            if (listView.SelectedItems.Count == 0) return;

            // Display confirmation dialog.
            if (MessageBox.Show(listView, "Are you sure you want to remove the selected movie ?",
                "MovieTracker", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var movie = listView.GetSelectedMovie();

                moviePanel.Movie = null;
                listView.RemoveMovie(listView.GetSelectedMovie());
                movieCollection.DeleteMovie(movie.Id);
            }
        }
        
        private void SearchMovie()
        {
            if (frmSearchMovie.Visible) return;

            frmSearchMovie = new FormSearchMovie();

            frmSearchMovie.bind(listView);
            frmSearchMovie.StartPosition = FormStartPosition.Manual;
            frmSearchMovie.SetDesktopLocation(this.Left + 25, this.Bottom - 150);
            frmSearchMovie.Show(this);
        }

        /// <summary>
        /// Load operation that executed on a separate thread.
        /// </summary>
        private void DoLoadMovies()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => listView.LoadMovies()));
            }
            else
            {
                this.listView.LoadMovies();
            }

            // Hide the Wait Form.
            frmWaitResult.Invoke(new MethodInvoker(() => frmWaitResult.Hide()));
        }

        #endregion

        private void splitContainer1_DoubleClick(object sender, EventArgs e)
        {
            if (splitContainer1.Panel2MinSize != 0)
            {
                splitContainer1.Panel2MinSize = 0;
                splitContainer1.SplitterDistance = splitContainer1.Width - splitContainer1.SplitterWidth;
            }
            else
            {
                splitContainer1.Panel2MinSize = 25;
                splitContainer1.SplitterDistance = splitContainer1.Width - 510;
            }
            
            listView.Select();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmSearchMovie.Close();
        }
    }
}
