﻿using System;
using System.Windows.Forms;
using MovieTracker.NetFetch;
using MovieTracker.Movies;
using System.Threading;
using System.Drawing;
using MovieTracker.Utils;


namespace MovieTracker.UserInterface
{
    /// <summary>
    /// FormAddMovie class.
	/// 	Form is used to add a new Movie to the Collection.
    /// </summary>
    public partial class FormAddMovie : Form
    {
        #region Members
        
        // Form which displays list of movies found after performing a search on website.
        private FormSearchResult formSearchResult = new FormSearchResult();

        // Form that is displayed while the user of slow operation.
        private FormWaitResult formWaitResult = new FormWaitResult();
        

        // Responsable for managing the NetFetchers(aka Plugins).
        private NetFetchManager netFetchManager = null;

        // Responsable for managing the collection of movies.
        private MovieCollection movieCollection = null;
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public FormAddMovie()
        {
            InitializeComponent();
        }

        #endregion

        #region Injector

        /// <summary>
        /// 
        /// </summary>
        public void bind(MovieCollection movieCollection, NetFetchManager netFetchManager)
        {
            this.movieCollection = movieCollection;
            this.netFetchManager = netFetchManager;

            this.moviePanel.bind(movieCollection, netFetchManager);
        }

        #endregion

        #region Properties
        
        /// <summary>
        /// Gets the movie that was added to the collection by the user.
        /// </summary>
        public Movie Movie
        {
            get; private set;
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the "Add" button`s click event.
        /// </summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddMovie();
        }

        #endregion

        /// <summary>
        /// Handles the "Cancel" button`s click event.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
                        
        #region Methods

        /// <summary>
        /// Shows the form as a modal dialog box.
        /// </summary>
        public new DialogResult ShowDialog()
        {
            this.moviePanel.ClearPanel();   // Clear the previous stuff from the form.

            return base.ShowDialog();
        }
        
        /// <summary>
        /// 
        /// </summary>
        private void AddMovie()
        {
            // Verify the title. A movie  must at least have a title before
            // it can be added to the collection.
            if (this.moviePanel.Title == String.Empty)
            {
                Utility.ShowMessage("Please enter the tilte of the movie you want do add!");

                return;
            }

            // Verify the type. The user must also specifie the type of the movie,
            // by selection one from the ComboBox.
            if (this.moviePanel.Type == -1)
            {
                MessageBox.Show("Please select the type of the movie!");

                return;
            }

            this.Movie = this.movieCollection.AddMovie(moviePanel.Title,
                                                moviePanel.Type,
                                                moviePanel.Genre,
                                                moviePanel.Director,
                                                moviePanel.Cast,
                                                moviePanel.Description,
                                                moviePanel.Rating,
                                                Convert.StringToInt(moviePanel.Year),
                                                moviePanel.Url,
                                                moviePanel.Poster);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion

        private void FormAddMovie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }

}
