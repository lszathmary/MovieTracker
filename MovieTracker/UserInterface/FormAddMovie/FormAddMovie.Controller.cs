﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using MovieTracker.NetFetch;
using MovieTracker.Movies;
using System.Threading;
using static MovieTracker.NetFetch.NetFetcher;
using System.Drawing;

namespace MovieTracker
{

    /// <summary>
    /// FormAddMovie class.
	/// 	Form is used to add a  New movie to the collection.
    /// </summary>
    public class FormAddMovieController
    {
        // The reference to the parent form.
        private Form formParent = null;

        // Form which displays list of movies found after performing a search on website.
        private FormSearchResult formSearchResult = new FormSearchResult();

        // Form that is displayed while the user of slow operation.
        private FormWaitResult formWaitResult = new FormWaitResult();


        // Responsable for managing the NetFetchers(aka Plugins).
        private NetFetchManager netFetchManager = null;

        // Responsable for managing the collection of movies.
        private MovieCollection movieCollection = null;
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movieCollection"></param>
        /// <param name="netFetchManager"></param>
        public void bind(Form form, MovieCollection movieCollection, NetFetchManager netFetchManager)
        {
            this.formParent = form;
            this.movieCollection = movieCollection;
            this.netFetchManager = netFetchManager;
        }
               
        /// <summary>
        /// Search for the movie thas has the title specified the by user.
        /// </summary>
        public void SearchMovie(String title)
        {
            // Execute the following operation on another thread:
            //  Perform the search on the website for the title introduced by the user.
            new Thread( () => DoSearchMovie(title) ).Start();

            // Display a form while background operation is performed.
            formWaitResult.ShowDialog();
        }

        /// <summary>
        /// Search operation that will be executed on a separate thread.
        /// </summary>
        private void DoSearchMovie(String title)
        {
            // Search for the specified title on the website.
            netFetchManager.Search(title);
            
            // Hide the Wait Form.
            formWaitResult.Invoke( new MethodInvoker(() => formWaitResult.Hide() ));
        }

        /// <summary>
        /// Search information for the movie selected by the user.
        /// </summary>
        private void FetchMovie(String url)
        {
            new Thread( () => DoSearchMovie(url) ).Start();

            // Display a form while background operation is performed.
            formWaitResult.ShowDialog();
        }

        /// <summary>
        /// Fetch operation that is executed on a separate thread.
        /// </summary>
        private void DoFetchMovie(String url)
        {
            // Fetch information from website about the specified movie.
            netFetchManager.Fetch(url);
            
            // Hide the Wait Form.
            formWaitResult.Invoke( new MethodInvoker(() => formWaitResult.Hide() ));
        }

        /// <summary>
        /// Load the search result of the netFetchManager into the ResultForm.
        /// </summary>
        private void LoadSearchResult()
        {
            // Remove previous search result from the dialog.
            formSearchResult.Clear();

            foreach (var result in this.netFetchManager.Results)
            {
                // Add search current result to the dialog.
                formSearchResult.AddMovie(result.Title, result.Description, result.Url);
            }
        }

        public void AddMovie(String title, int type, String genre, String director, String cast,
                    String description, Decimal rating, int year, String url, Image poster)
        {
            
        }

    }
}