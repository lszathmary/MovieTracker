﻿using MovieTracker.Movies;
using MovieTracker.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MovieTracker.UserInterface
{
    public partial class FormCSVImport : Form
    {
        #region Members

        // Form that is displayed while the user of slow operation.
        private FormWaitResult formWaitResult = new FormWaitResult();

        // Contains the content of the CSV File
        private DataTable dataTable = null;

        // Responsable for managing the movie collection.
        private MovieCollection movieCollection = null;

        //
        private MovieListView movieListView = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public FormCSVImport()
        {
            InitializeComponent();

            // Set default values.
            chkBoxImportHeader.Checked = true;
            tBoxSeparator.Text = ";";
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the "Select" button`s click event.
        /// </summary>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Import the Data From the CSV File to a DataTable.
                dataTable = CSVFileToDataTable(openFileDialog1.FileName, chkBoxImportHeader.Checked, tBoxSeparator.Text);

                // Display the FileName in a TextBox and the content in a DataGridView.
                tBoxFileName.Text = openFileDialog1.FileName;
                dataGridView1.DataSource = dataTable;   
                
                // Set up the ComboBox`s located on the Form.
                this.ClearCombo();

                foreach (DataColumn column in dataTable.Columns)
                {
                    this.AddToCombo(column.ColumnName);
                }
            }
        }
               
        /// <summary>
        /// Handles the "Import" button`s click event.
        /// </summary>
        private void btnImport_Click(object sender, EventArgs e)
        {
            if (dataTable == null)
            {
                Utility.ShowMessage("Please select the file from which to import the data first!");

                return;
            }

            // Verify the type. The user must also specifie the type of the movie,
            // by selection one from the ComboBox.
            if (this.cboxType.SelectedIndex == -1)
            {
                MessageBox.Show("Please select the type of the movie!");

                return;
            }

            // Verify the title. A movie  must at least have a title before
            // it can be added to the collection.
            if (cBoxTitle.SelectedItem == null)
            {
                Utility.ShowMessage("Please select the column from which to extract the Title !");

                return;
            }


            // Execute the following operation on another thread:
            //  Perform the search on the website for the title introduced by the user.
            new Thread(() => _Import()).Start();


            // Display a form while background operation is performed.
            formWaitResult.ShowDialog();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clears the ComboBoxes located on the Form.
        /// </summary>
        private void ClearCombo()
        {
            cBoxTitle.Items.Clear();
            cBoxDirector.Items.Clear();
            cBoxGenre.Items.Clear();
            cBoxCast.Items.Clear();
            cBoxRating.Items.Clear();
            cBoxYear.Items.Clear();
            cBoxDescription.Items.Clear();
            cBoxWebPage.Items.Clear();
            cBoxPoster.Items.Clear();
        }

        /// <summary>
        /// Add the specified item to the ComboBoxes located on the Form
        /// </summary>
        private void AddToCombo (String item)
        {
            cBoxTitle.Items.Add(item);
            cBoxDirector.Items.Add(item);
            cBoxGenre.Items.Add(item);
            cBoxCast.Items.Add(item);
            cBoxRating.Items.Add(item);
            cBoxYear.Items.Add(item);
            cBoxDescription.Items.Add(item);
            cBoxWebPage.Items.Add(item);
            cBoxPoster.Items.Add(item);
        }

        /// <summary>
        /// Extracts the content of a CVS File into a DataTable.
        /// </summary>
        private DataTable CSVFileToDataTable(String fileName, bool isFirstRowHeader, String delimiter)
        {
            DataTable dataTable = new DataTable();

            String path = Path.GetDirectoryName(fileName);
            String file = Path.GetFileName(fileName);
            String query = @"SELECT * FROM [" + file + "]";
            String header = isFirstRowHeader ? "True" : "False";

            try
            {
                // Create schema file in which the delimiter character is specified
                using (FileStream stream = File.Open(Path.Combine(path, "schema.ini"), FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.WriteLine(string.Format("[{0}]", file));
                        writer.WriteLine("Format=Delimited(" + delimiter + ")");
                        writer.WriteLine("TextDelimiter='");
                        writer.WriteLine("ColNameHeader=" + header);
                    }
                }

                // Create the connection string, establish connection and fill the DataTable.
                String connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
                                + path + ";Extended Properties=\"Text;" + ";FMT=Delimited(" + delimiter + ")\"";

                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    using (OleDbCommand command = new OleDbCommand(query, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.ShowMessage(ex.Message);
            }

            return dataTable;
        }

        private void _Import()
        {
            // Each row contains information about a different movie.
            for (int index = 0; index < 10; index++)
            {
                String title = getTitle(dataTable.Rows[index]);
                String genre = getGenre(dataTable.Rows[index]);
                String director = getDirector(dataTable.Rows[index]);
                String cast = getCast(dataTable.Rows[index]);
                Decimal rating = getRating(dataTable.Rows[index]);
                int year = getYear(dataTable.Rows[index]);
                String description = getDescription(dataTable.Rows[index]);
                String webPage = getWebPage(dataTable.Rows[index]);
                String posterFileName = Path.GetDirectoryName(openFileDialog1.FileName) + "\\" + getPosterFileName(dataTable.Rows[index]);
                
                if (title != String.Empty)
                {
                    movieCollection.AddMovie(title, 
                                        cboxType.SelectedIndex, 
                                        genre, 
                                        director, 
                                        cast,
                                        description, 
                                        rating, 
                                        year, 
                                        webPage, 
                                        LoadPosterFromFile(posterFileName));
                }
            }
        }

        /// <summary>
        /// Extracts the Title from the specified DataRow.
        /// </summary>
        private String getTitle(DataRow dataRow)
        {
            if (cBoxTitle.SelectedItem != null)
            {
                return dataRow[cBoxTitle.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Extracts the Genre from the specified DataRow.
        /// </summary>
        private String getGenre(DataRow dataRow)
        {
            if (cBoxGenre.SelectedItem != null)
            {
                return dataRow[cBoxGenre.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }
        
        /// <summary>
        /// Extracts the Director from the specified DataRow.
        /// </summary>
        private String getDirector(DataRow dataRow)
        {
            if (cBoxDirector.SelectedItem != null)
            {
                return dataRow[cBoxDirector.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Extracts the Cast from the specified DataRow.
        /// </summary>
        private String getCast(DataRow dataRow)
        {
            if (cBoxCast.SelectedItem != null)
            {
                return dataRow[cBoxCast.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Extracts the Rating from the specified DataRow.
        /// </summary>
        private Decimal getRating(DataRow dataRow)
        {
            if (cBoxRating.SelectedItem != null)
            {
                String str = dataRow[cBoxRating.SelectedItem.ToString()].ToString();

                return Convert.StringToDecimal(str);
            }

            return 0;
        }

        /// <summary>
        /// Extracts the Year from the specified DataRow.
        /// </summary>
        private int getYear(DataRow dataRow)
        {
            if (cBoxYear.SelectedItem != null)
            {
                String str = dataRow[cBoxYear.SelectedItem.ToString()].ToString();

                return Convert.StringToInt(str);
            }

            return 0;
        }

        /// <summary>
        /// Extracts the Description from the specified DataRow.
        /// </summary>
        private String getDescription(DataRow dataRow)
        {
            if (cBoxDescription.SelectedItem != null)
            {
                return dataRow[cBoxDescription.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Extracts the WebPage from the specified DataRow.
        /// </summary>
        private String getWebPage(DataRow dataRow)
        {
            if (cBoxWebPage.SelectedItem != null)
            {
                return dataRow[cBoxWebPage.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Extracts the WebPage from the specified DataRow.
        /// </summary>
        private String getPosterFileName(DataRow dataRow)
        {
            if (cBoxPoster.SelectedItem != null)
            {
                return dataRow[cBoxPoster.SelectedItem.ToString()].ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Load the poster from the specified file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private Image LoadPosterFromFile(String fileName)
        {
            try
            {
                Stream stream = File.OpenRead(fileName);
               
                 return System.Drawing.Image.FromStream(stream);
               
            }
            catch (Exception) { }

            return null;
        }

        /// <summary>
        /// Bind the different component to this Form.
        /// </summary>
        public void bind(MovieCollection movieCollection, MovieListView movieListView)
        {
            this.movieCollection = movieCollection;
            this.movieListView = movieListView;
        }

        #endregion
    }
}
