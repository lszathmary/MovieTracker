﻿namespace MovieTracker.UserInterface
{
    partial class FormCSVImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tBoxFileName = new System.Windows.Forms.TextBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cBoxPoster = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboxType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cBoxWebPage = new System.Windows.Forms.ComboBox();
            this.cBoxDescription = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cBoxYear = new System.Windows.Forms.ComboBox();
            this.cBoxRating = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cBoxCast = new System.Windows.Forms.ComboBox();
            this.cBoxGenre = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cBoxDirector = new System.Windows.Forms.ComboBox();
            this.cBoxTitle = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnImport = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tBoxSeparator = new System.Windows.Forms.TextBox();
            this.chkBoxImportHeader = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Import File:";
            // 
            // tBoxFileName
            // 
            this.tBoxFileName.Location = new System.Drawing.Point(109, 18);
            this.tBoxFileName.Name = "tBoxFileName";
            this.tBoxFileName.ReadOnly = true;
            this.tBoxFileName.Size = new System.Drawing.Size(413, 20);
            this.tBoxFileName.TabIndex = 1;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(565, 18);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(41, 23);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "...";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cBoxPoster);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cboxType);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cBoxWebPage);
            this.groupBox1.Controls.Add(this.cBoxDescription);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cBoxYear);
            this.groupBox1.Controls.Add(this.cBoxRating);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cBoxCast);
            this.groupBox1.Controls.Add(this.cBoxGenre);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cBoxDirector);
            this.groupBox1.Controls.Add(this.cBoxTitle);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(15, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(598, 199);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Columns";
            // 
            // cBoxPoster
            // 
            this.cBoxPoster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxPoster.FormattingEnabled = true;
            this.cBoxPoster.Location = new System.Drawing.Point(367, 163);
            this.cBoxPoster.Name = "cBoxPoster";
            this.cBoxPoster.Size = new System.Drawing.Size(200, 21);
            this.cBoxPoster.TabIndex = 88;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(298, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 87;
            this.label12.Text = "Poster:";
            // 
            // cboxType
            // 
            this.cboxType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxType.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboxType.FormatString = "  ";
            this.cboxType.FormattingEnabled = true;
            this.cboxType.Items.AddRange(new object[] {
            "Movie",
            "Documentary",
            "Mini-Series"});
            this.cboxType.Location = new System.Drawing.Point(73, 19);
            this.cboxType.Name = "cboxType";
            this.cboxType.Size = new System.Drawing.Size(200, 21);
            this.cboxType.TabIndex = 86;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 85;
            this.label11.Text = "Type:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(298, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "WebPage:";
            // 
            // cBoxWebPage
            // 
            this.cBoxWebPage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxWebPage.FormattingEnabled = true;
            this.cBoxWebPage.Location = new System.Drawing.Point(367, 127);
            this.cBoxWebPage.Name = "cBoxWebPage";
            this.cBoxWebPage.Size = new System.Drawing.Size(200, 21);
            this.cBoxWebPage.TabIndex = 15;
            // 
            // cBoxDescription
            // 
            this.cBoxDescription.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxDescription.FormattingEnabled = true;
            this.cBoxDescription.Location = new System.Drawing.Point(367, 90);
            this.cBoxDescription.Name = "cBoxDescription";
            this.cBoxDescription.Size = new System.Drawing.Size(200, 21);
            this.cBoxDescription.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(298, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Description:";
            // 
            // cBoxYear
            // 
            this.cBoxYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxYear.FormattingEnabled = true;
            this.cBoxYear.Location = new System.Drawing.Point(367, 53);
            this.cBoxYear.Name = "cBoxYear";
            this.cBoxYear.Size = new System.Drawing.Size(200, 21);
            this.cBoxYear.TabIndex = 12;
            // 
            // cBoxRating
            // 
            this.cBoxRating.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxRating.FormattingEnabled = true;
            this.cBoxRating.Location = new System.Drawing.Point(367, 19);
            this.cBoxRating.Name = "cBoxRating";
            this.cBoxRating.Size = new System.Drawing.Size(200, 21);
            this.cBoxRating.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(298, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Year:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(298, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Rating:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Cast:";
            // 
            // cBoxCast
            // 
            this.cBoxCast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxCast.FormattingEnabled = true;
            this.cBoxCast.Location = new System.Drawing.Point(73, 163);
            this.cBoxCast.Name = "cBoxCast";
            this.cBoxCast.Size = new System.Drawing.Size(200, 21);
            this.cBoxCast.TabIndex = 7;
            // 
            // cBoxGenre
            // 
            this.cBoxGenre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxGenre.FormattingEnabled = true;
            this.cBoxGenre.Location = new System.Drawing.Point(73, 127);
            this.cBoxGenre.Name = "cBoxGenre";
            this.cBoxGenre.Size = new System.Drawing.Size(200, 21);
            this.cBoxGenre.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Genre:";
            // 
            // cBoxDirector
            // 
            this.cBoxDirector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxDirector.FormattingEnabled = true;
            this.cBoxDirector.Location = new System.Drawing.Point(73, 90);
            this.cBoxDirector.Name = "cBoxDirector";
            this.cBoxDirector.Size = new System.Drawing.Size(200, 21);
            this.cBoxDirector.TabIndex = 3;
            // 
            // cBoxTitle
            // 
            this.cBoxTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxTitle.FormattingEnabled = true;
            this.cBoxTitle.Location = new System.Drawing.Point(73, 53);
            this.cBoxTitle.Name = "cBoxTitle";
            this.cBoxTitle.Size = new System.Drawing.Size(200, 21);
            this.cBoxTitle.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Director:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Title:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(17, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(563, 218);
            this.dataGridView1.TabIndex = 4;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(266, 575);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(105, 23);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(15, 313);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(598, 256);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Content";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tBoxSeparator);
            this.groupBox3.Controls.Add(this.chkBoxImportHeader);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(15, 47);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(598, 55);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Options:";
            // 
            // tBoxSeparator
            // 
            this.tBoxSeparator.Location = new System.Drawing.Point(473, 18);
            this.tBoxSeparator.MaxLength = 1;
            this.tBoxSeparator.Name = "tBoxSeparator";
            this.tBoxSeparator.Size = new System.Drawing.Size(34, 20);
            this.tBoxSeparator.TabIndex = 10;
            // 
            // chkBoxImportHeader
            // 
            this.chkBoxImportHeader.AutoSize = true;
            this.chkBoxImportHeader.Location = new System.Drawing.Point(94, 21);
            this.chkBoxImportHeader.Name = "chkBoxImportHeader";
            this.chkBoxImportHeader.Size = new System.Drawing.Size(162, 17);
            this.chkBoxImportHeader.TabIndex = 9;
            this.chkBoxImportHeader.Text = "Import file contains a header.";
            this.chkBoxImportHeader.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(352, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Separator Character:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormCSVImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 618);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.tBoxFileName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCSVImport";
            this.Text = "CSV Import";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tBoxFileName;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cBoxDirector;
        private System.Windows.Forms.ComboBox cBoxTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ComboBox cBoxGenre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cBoxWebPage;
        private System.Windows.Forms.ComboBox cBoxDescription;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cBoxYear;
        private System.Windows.Forms.ComboBox cBoxRating;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cBoxCast;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tBoxSeparator;
        private System.Windows.Forms.CheckBox chkBoxImportHeader;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox cboxType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cBoxPoster;
        private System.Windows.Forms.Label label12;
    }
}