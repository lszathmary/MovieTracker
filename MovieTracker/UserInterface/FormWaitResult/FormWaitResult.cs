﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MovieTracker.UserInterface
{
	/// <summary>
    ///  FormWaitResult class.
	///		This dialog is displayed, while user has to wait for a longer period,
	/// for example whan data is fetched from the net, or information is loaded from
	/// the database.
    /// </summary>
    public partial class FormWaitResult : Form
    {
        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public FormWaitResult()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterParent;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Display the dialog with the specified title.
        /// </summary>
        public void ShowDialog(String title)
        {
            // Set the title of the form.
            this.Text = title;

            // Display the form as modal dialog box.
            this.ShowDialog();
        }

        #endregion
    }
}
