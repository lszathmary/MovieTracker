﻿using System;
using System.Windows.Forms;


namespace MovieTracker.UserInterface
{
    /// <summary>
    /// FormSearchResult.
    ///     This form is used to display the list of movies found by the NetFetchManager.
    /// </summary>
    public partial class FormSearchResult : Form
    {
        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public FormSearchResult()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The Url of the movie that the user selected.
        /// </summary>
        public String URL
        {
            get; private set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clear the previous data from the Form.
        /// </summary>
        public void Clear()
        {
            this.listView.Items.Clear();
        }

        /// <summary>
        /// Add a movie to the Form.
        /// </summary>
        public void AddResult(String title, String description, String url)
        {
            var item = listView.Items.Add(title); 
            
            item.SubItems.Add(description);     // Show description in second column.
            item.Tag = url;                     // Store url of the movie in the Tag.
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the ListView`s double click event.
        /// </summary>
        private void listView_DoubleClick(object sender, EventArgs e)
        {
            // Get the Url of the currently selected result and close the Form.
            if (listView.SelectedItems.Count == 1)
            {
                this.URL = listView.SelectedItems[0].Tag as String;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        #endregion
    }
}
