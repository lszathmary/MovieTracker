﻿using System;
using System.Data.SQLite;
using System.Data;
using System.IO;
using MovieTracker.Utils;
using System.Windows.Forms;

namespace MovieTracker.DataBase
{
    /// <summary>
    /// DataBaseManager class.
    ///     The class is responsabile for interacting with the database. Reading, writing and updating data 
    /// from/to the SQLite data base is done through this class.
    /// </summary>
    public class DataBaseManager
    {
        // Represents an open connection to a SQLite database. 
        private SQLiteConnection connection;

        /// <summary>
        /// Constructor. Establishes connection with the database.
        /// </summary>
        public DataBaseManager()
        {
            // NOTE:
            //  The SQLite database named "collection.sqlite" should be located 
            //  in the folder named collection.

            var connectionString = "Data Source=" + Directory.GetCurrentDirectory() + @"\collection\collection.sqlite";
                        
            try
            {
                // Create the new connection to the database file.  
                connection = new SQLiteConnection(connectionString);

                // Open the connection to the database.
                connection.Open();
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Extracts information about movie with the specified id from the database into a datatable.
        /// </summary>
        public DataTable GetMovie(int id)
        {
            DataTable dataTable = new DataTable();

            // Create a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();
            
            // Prepare the query for selecting the movie with the specified id.            
            command.CommandText = @"SELECT * FROM T_MOVIE WHERE Id = " + id.ToString();

            try
            {
                // Execute the SQL query and read the data from the database
                // than load it into a datatable.
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataTable.Load(dataReader);
                }
            }
            catch(Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }

            return dataTable;
        }

        /// <summary>
        /// Extracts available information about all the movies from the database into a datatable.
        /// </summary>
        public DataTable GetMovies()
        {
            DataTable dataTable = new DataTable();

            // Create a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Prepare the query for selecting all the movies.
            command.CommandText = @"SELECT * FROM T_MOVIE";

            try
            {
                // Execute the SQL query and read the data from the database
                // than load it into a datatable.
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataTable.Load(dataReader);
                }
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }

            return dataTable;
        }

        /// <summary>
        /// Extracts the list of movies that match the specified criteria.
        /// </summary>
        public DataTable SearchMovies(String title, String genre, String director, int year)
        {
            DataTable dataTable = new DataTable();

            // Create a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Prepare the query for selecting all the movies.
            command.CommandText = @"SELECT * FROM T_MOVIE WHERE Title LIKE @Title"
                                  + " AND Genre LIKE @Genre"
                                  + " AND Director LIKE @Director";
                  

            // Set the parameters of the query.
            command.Parameters.AddWithValue("@Title", "%" + title + "%");
            command.Parameters.AddWithValue("@Genre", "%" + genre + "%");
            command.Parameters.AddWithValue("@Director", "%" + director + "%");

            if (year != 0)
            {
                command.CommandText += " AND Year = " + year.ToString();
            }
            
            try
            {
                
                // Execute the SQL query and read the data from the database
                // than load it into a datatable.
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataTable.Load(dataReader);
                }
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }
                     

            return dataTable;
        }

        /// <summary>
        /// Gets the number of movies present in the database.
        /// </summary>
        public int GetMovieCount()
        {
            int count = -1;

            // Creates a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Prepare the query for selecting all the movies.
            command.CommandText = @"SELECT COUNT(Id) FROM T_MOVIE";

            try
            {
                count = int.Parse(command.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }

            return count;
        }
        
        /// <summary>
        /// Insert a new movie information into the database.
        /// </summary>
        public int InsertMovie(String title, int type, String genre, String director, String cast, 
                        String description, Decimal rating, int year, String url)
        {
            int id = -1;

            // Create a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Prepare the query for inserting the movie information in the database
            // and extract the id of the newly inserted row.
            command.CommandText = @"INSERT INTO T_MOVIE(Title, Type, Genre, Director, Cast, Description, Rating, Year, URL)"
                             + "VALUES (@Title, @Type, @Genre, @Director, @Cast, @Description, @Rating, @Year, @url);"
                             + "SELECT last_insert_rowid()";
                        
            // Set the parameters of the query.
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@Type", type);
            command.Parameters.AddWithValue("@Genre", genre);
            command.Parameters.AddWithValue("@Director", director);
            command.Parameters.AddWithValue("@Cast", cast);
            command.Parameters.AddWithValue("@Description", description);
            command.Parameters.AddWithValue("@Rating", rating);
            command.Parameters.AddWithValue("@Year", year);
            command.Parameters.AddWithValue("@URL", url);

            try
            {
                // Execute the SQL query and then read id of the newly inserted row.
                id = int.Parse(command.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }

            return id;
        }

        /// <summary>
        /// Update the filenames (posterFileName, thumbnailFileName) of the specified movie in the database.
        /// </summary>
        public void UpdateFileName(int id, String posterFileName, String thumbnailFileName)
        {
            // Create. a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Build the update query.            
            command.CommandText = " UPDATE T_MOVIE "
                             + " SET PosterFileName = @PosterFileName, ThumbnailFileName = @ThumbnailFileName"
                             + " WHERE Id = @Id ";

            // Set the parameters of the query.
            command.Parameters.AddWithValue("@PosterFileName", posterFileName);
            command.Parameters.AddWithValue("@ThumbnailFileName", thumbnailFileName);
            command.Parameters.AddWithValue("@Id", id);

            try
            {
                // Execute the SQL query.
                command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Update the movie with the specfied id.
        /// </summary>
        public void UpdateMovie(int id, String title, int type, String genre, String director, String cast, 
                        String description, Decimal rating, int year, String url)
        {
            // Create a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Prepare the query for updating the row with the specified id.
            command.CommandText = " UPDATE T_MOVIE "
                             + " SET Title = @Title, Type = @Type, Genre = @Genre, Director = @Director, Cast = @Cast,"
                             + " Description = @Description, Rating = @Rating, Year = @Year, URL = @URL"
                             + " WHERE Id = @Id ";

            // Set the parameters of the query.
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@Type", type);
            command.Parameters.AddWithValue("@Genre", genre);
            command.Parameters.AddWithValue("@Director", director);
            command.Parameters.AddWithValue("@Cast", cast);
            command.Parameters.AddWithValue("@Description", description);
            command.Parameters.AddWithValue("@Rating", rating);
            command.Parameters.AddWithValue("@Year", year);
            command.Parameters.AddWithValue("@URL", url);
            command.Parameters.AddWithValue("@Id", id);

            try
            {
                // Execute the SQL query updating the row with the specified id.
                command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }
        }
        
        /// <summary>
        /// Delete the movie information with the specfied id from the database.
        /// </summary>
        public void DeleteMovie(int id)
        {
            // Creates a new SQLite command.
            SQLiteCommand command = connection.CreateCommand();

            // Prepare the query for deleting the row with the specified id.
            command.CommandText = "DELETE FROM T_MOVIE WHERE Id = " + id.ToString();

            try
            {
                // Execute the SQL query deleting the row with the specified id.
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Utility.ShowErrorMessage(ex.Message);
            }
        }
        
    }
}
