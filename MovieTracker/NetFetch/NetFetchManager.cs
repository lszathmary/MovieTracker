﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using static MovieTracker.NetFetch.NetFetcher;
using MovieTracker.Utils;

namespace MovieTracker.NetFetch
{
    /// <summary>
    /// NetFetchManager class.
    ///     Responsable for managing the list of available NetFetchers.
    /// </summary>
    public class NetFetchManager
    {
        // The list of available NetFetchers.
        private Dictionary<String,NetFetcher> netFetcherList;
		
        // The currently active plugin.
        private NetFetcher activeFetcher;

        /// <summary>
        /// 
        /// </summary>
        public List<Result> Results
        {
            get
            {
                return this.activeFetcher.Results;
            }
            private set { }
        }

        /// <summary>
        /// The title of the movie.
        /// </summary>
        public String Title
        {
            get
            {
                return this.activeFetcher.Title;
            }
            private set { }
        }
        
        /// <summary>
        /// The directors of the movie.
        /// </summary>
        public String Director
        {
            get
            {
                return Convert.ListToString(this.activeFetcher.Directors);
            }
            private set { }
        }
              
        /// <summary>
        /// The genres of the movie.
        /// </summary>
        public String Genre
        {
            get
            {
                return Convert.ListToString(this.activeFetcher.Genres);
            }
        
            private set { }
        }

        /// <summary>
        /// The cast of the movie.
        /// </summary>
        public String Cast
        {
            get
            {
                return Convert.ListToString(this.activeFetcher.Cast);
            }
        
            private set { }
        }

        /// <summary>
        /// The description of the movie.
        /// </summary>
        public String Description
        {
            get
            {
                return this.activeFetcher.Description;
            }
            private set { }
        }

        /// <summary>
        /// The rating of the movie.
        /// </summary>
        public Decimal Rating
        {
            get
            {
                return Convert.StringToDecimal(this.activeFetcher.Rating);
            }
            private set { }
        }

        /// <summary>
        /// The year the movie was released.
        /// </summary>
        public String Year
        {
            get
            {
                return this.activeFetcher.Year;
            }
            private set { }
        }
        
        /// <summary>
        /// An URL to the movie poster.
        /// </summary>
        public String PosterURL
        {
            get
            {
                return this.activeFetcher.PosterURL;
            }

            private set { }
        }
                
        /// <summary>
        /// The poster of the movie.
        /// </summary>
        public Image Poster
        {
            get
            {
                return this.activeFetcher.Poster;
            }

            private set { }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Url
        {
            get; private set;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public NetFetchManager()
        {
            this.netFetcherList = new Dictionary<String, NetFetcher>();
                        
            // Get the list of csharp files located in the plugins directory.
            String pluginDirectory = Directory.GetCurrentDirectory() + @"\plugins";
            String[] pluginFiles = Directory.GetFiles(pluginDirectory, "*.cs");
                                    
            foreach (String pluginFile in pluginFiles)
            {
                // Create the NetFetcher from the specified C# file and add it a dictionary.
                NetFetcher netFetcher = new NetFetcher(pluginFile);

                if (netFetcher.Name != null)
                {
                    netFetcherList.Add(netFetcher.Name, netFetcher);
                }
            }

            // Display a warning message if no plugins were loaded.
            if (netFetcherList.Count == 0)
            {
                MessageBox.Show("Warning. There were no plugins loaded !",
                    "MovieTracker", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                // Get the first NetFetcher in the dictionary and set it active.
                foreach (var entry in netFetcherList)
                {
                    this.activeFetcher = entry.Value;  break;
                }
            }
        }

        /// <summary>
        /// Search the website for the specified movie title.
        /// The search result is in TitleList, DescriptionList, and UrlList.
        /// </summary>
        public bool Search(string title)
        {
            return this.activeFetcher.Search(title);
        }

        /// <summary>
        /// Fetch information about the movie from the specified url.
        /// </summary>
        /// <param name="URL"></param>
        public void Fetch(String URL)
        {
            this.Url = URL;
            
            activeFetcher.Fetch(this.Url);
        }

        /// <summary>
        /// Get the names of the available NetFetchers.
        /// </summary>
        /// <returns></returns>
        public String[] GetNetFetchers()
        {
            List<String> nameList = new List<string>();

            foreach (var entry in netFetcherList)
            {
                nameList.Add(entry.Key);
            }

            return nameList.ToArray();
        }

        /// <summary>
        /// Set the active NetFetcher.
        /// </summary>
        /// <param name="name">Name of the NetFetcher that will be set active.</param>
        public void SetActiveNetFetcher(String name)
        {
            this.activeFetcher = netFetcherList[name];
        }

        /// <summary>
        /// Gets the name of the currently active NetFetcher.
        /// </summary>
        public String GetActiveNetFetcher()
        {
            return this.activeFetcher.Name;
        }
    }
}
