﻿using System;
using System.Collections.Generic;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using MovieTracker.Utils;


namespace MovieTracker.NetFetch
{
    /// <summary>
    /// ScriptManager.
    ///     Responsable for compiling and running a C# script.
    /// </summary>
    public class ScriptRunner
    {
        // Provides access to instances of the C# code generator and code compiler.
        private CSharpCodeProvider codeProvider;

        // Represents the parameters used to invoke a compiler.
        private CompilerParameters compilerParameters;


        // An instance of the script.
        private Object script;


        /// <summary>
        /// Create an instance of the ScriptManager class.
        /// </summary>
        public ScriptRunner()
        {
            // Set up compiler options.
            Dictionary<string, string> options = new Dictionary<string, string>
            {
                { "CompilerVersion", "v2.0" }
            };

            codeProvider = new CSharpCodeProvider(options);
            compilerParameters = new CompilerParameters();
                      
            // The compiler should generate a dll in memory.
            compilerParameters.GenerateInMemory = true;
            compilerParameters.GenerateExecutable = false;
                   
            // Add references.
            compilerParameters.ReferencedAssemblies.Add("System.dll");
            compilerParameters.ReferencedAssemblies.Add("System.Windows.Forms.dll");
        }

        /// <summary>
        /// Compiles an assembly from the source code contained in the specified files.
        /// </summary>
        /// <param name="fileName">The name of the file that contains the script.</param>
        /// <returns>True if the sript compiles correctly otherwise false.</returns>
        public bool CompileScriptFromFile(String fileName)
        {
            try
            {
                // Complie the script file
                CompilerResults results = codeProvider.CompileAssemblyFromFile(compilerParameters, fileName);

                // Check for errors
                if (results.Errors.Count > 0)
                {
                    String errorMessage = "An error occured while building the following scipt: " + fileName;

                    // Build the error message.
                    for (int x = 0; x < results.Errors.Count; x++)
                    {
                        errorMessage = errorMessage + "\r\r\nLine: " +
                                     results.Errors[x].Line.ToString() + " - " +
                                     results.Errors[x].ErrorText + "\r\n\r\n";

                        // Display the error message
                        Utility.ShowErrorMessage(errorMessage);
                    }

                    return false;
                }
                else
                {
                    // Create an instance of the Script.
                    script = results.CompiledAssembly.CreateInstance("Script");
                }
            }
            catch (Exception e)
            {
                Utility.ShowErrorMessage(e.ToString());
            }

            return true;
        }

        /// <summary>
        /// Returns the value of the specified property.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>The value of the property.</returns>
        public Object GetProperty(String propertyName)
        {
            Object value = null;

            try
            {
                value = script.GetType().GetProperty(propertyName).GetValue(script, null);
            }
            catch(Exception e)
            {
                Utility.ShowErrorMessage(e.ToString());
            }

            return value;
        }

        /// <summary>
        /// Invokes/execute the specified method.
        /// </summary>
        /// <param name="methodName">The name of the method to execute.</param>
        /// <param name="parameters">List of the methods parameters.</param>
        public void InvokeMethod(string methodName, object[] parameters)
        {
            try
            {
                script.GetType().GetMethod(methodName).Invoke(script, parameters);
            }
            catch (Exception e)
            {
                Utility.ShowErrorMessage(e.ToString());
            }
        }
        
    }
}
