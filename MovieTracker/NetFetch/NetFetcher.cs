﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace MovieTracker.NetFetch
{
    /// <summary>
    /// NetFetcher class.
    ///     Responsable for retrieving information about movie from a specific websites(for example imdb).
    /// 
    /// </summary>
    public class NetFetcher
    {
        /// <summary>
        /// The result received after calling the Search() method.
        /// </summary>
        public class Result
        {
            /// <summary>
            /// Title of the movie.
            /// </summary>
            public String Title
            {
                get; internal set;
            }

            /// <summary>
            /// Description of the movie.
            /// </summary>
            public String Description
            {
                get; internal set;
            }
            
            /// <summary>
            /// URL of the movie.
            /// </summary>
            public String Url
            {
                get; internal set;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            internal Result(String title, String description, String url)
            {
                this.Title = title;
                this.Description = description;
                this.Url = url;
            }
        }

        // Responsable for compiling and running a C# script.
        private ScriptRunner scriptRunner;

        // Provides common methods for sending data to and receiving data from a resource identified by a URL.
        private WebClient webClient;

        // The lis of movies found after perform a search.
        private List<Result> results = new List<Result>();
    

        /// <summary>
        /// The name of the netfetcher.
        /// </summary>
        public String Name
        {
            get; private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<Result> Results
        {
            get
            {
                return this.results;
            }
            private set { }
        }

        /// <summary>
        /// The title of the movie.
        /// </summary>
        public String Title
        {
            get; private set;
        }
        
        /// <summary>
        /// The directors of the movie.
        /// </summary>
        public List<String> Directors
        {
            get; private set;
        }

        /// <summary>
        /// The genres of the movie.
        /// </summary>
        public List<String> Genres
        {
            get; private set;
        }

        /// <summary>
        /// The description of the movie.
        /// </summary>
        public String Description
        {
            get; private set;
        }

        /// <summary>
        /// The directors of the movie.
        /// </summary>
        public List<String> Cast
        {
            get; private set;
        }

        /// <summary>
        /// The title of the movie.
        /// </summary>
        public String Rating
        {
            get; private set;
        }
        
        /// <summary>
        /// The title of the movie.
        /// </summary>
        public String Year
        {
            get; private set;
        }

        /// <summary>
        /// An URL to the movie poster.
        /// </summary>
        public String PosterURL
        {
            get; private set;
        }
              
        /// <summary>
        /// The poster of the movie.
        /// </summary>
        public Image Poster
        {
            get
            {
                return this.getImage(this.PosterURL);
            }

            private set { }
        }

        /// <summary>
        /// Create an instance of the NetFetcher class.
        /// </summary>
        public NetFetcher(String fileName)
        {
            scriptRunner = new ScriptRunner();
            
            // Initializes a new instance of the WebClient class.
            webClient = new WebClient();

            webClient.Headers["Accept-Language"] = "en-EN";
            webClient.Headers["User-Agent"] = @"Mozilla / 5.0(compatible; Googlebot / 2.1; +http://www.google.com/bot.html)";
            
            // Compile the plugin.
            if (scriptRunner.CompileScriptFromFile(fileName))
            {
                // Get the name of the plugin.
                this.Name = scriptRunner.GetProperty("Name") as String;
            }
        }

        /// <summary>
        /// Search the website for the specified movie title.
        /// The search result is in TitleList, DescriptionList, and UrlList.
        /// </summary>
        /// <param name="title">The title of the movie to search for.</param>
        /// <returns>True if something was found, otherwise false.</returns>
        public bool Search(string title)
        {
            // Read the searchPage/engine from the script.
            String searchPage = (String)scriptRunner.GetProperty("SearchPage");

            // Perform the search and download the website containing the search result.
            String site = this.getWebPage(searchPage + title);
            
            // Parse the search page using the scipt.          
            scriptRunner.InvokeMethod("parseSearchPage", new object[] { site } );
                        
            // Get the result (list of urls, titles, descriptions) from the script.
            List<String> titleList = scriptRunner.GetProperty("TitleList") as List<String>;
            List<String> descriptionList = scriptRunner.GetProperty("DescriptionList") as List<String>;
            List<String> urlList = scriptRunner.GetProperty("URLList") as List<String>;
            
            // Clear previously fetched data.
            this.Results.Clear();

            // Create the result list which contains the list of movies found.
            for (int index = 0; index < titleList.Count; index++)
            {
                this.Results.Add(new Result(titleList[index], descriptionList[index], urlList[index]));
            }

            return (urlList.Count > 0);
        }

        /// <summary>
        /// Fetch information about the movie from the specified url.
        /// </summary>
        /// <param name="URL"></param>
        public void Fetch(String URL)
        {
            String site = this.getWebPage(URL);

            // Parse the search page using the scipt.          
            scriptRunner.InvokeMethod("parseMoviePage", new object[] { site });

            this.Title = scriptRunner.GetProperty("Title") as String;
            this.Year = scriptRunner.GetProperty("Year") as String;
            this.Genres = scriptRunner.GetProperty("Genres") as List<String>;
            this.Description = scriptRunner.GetProperty("Description") as String;
            this.Directors = scriptRunner.GetProperty("Directors") as List<String>;
            this.PosterURL = scriptRunner.GetProperty("PosterURL") as String;
            this.Cast = scriptRunner.GetProperty("Cast") as List<String>;
            this.Rating = scriptRunner.GetProperty("Rating") as String;
        }

        /// <summary>
        /// Download the specified webpage.
        /// </summary>
        private String getWebPage(String url)
        {
            return webClient.DownloadString(url);
        }

        /// <summary>
        /// Download the image located at the specified url.
        /// </summary>
        /// <param name="url">The url that points to the image file.</param>
        private Image getImage(string url)
        {
            if ( (url != String.Empty) && (url != null) )
            {
                // Download the image into the a stream.
                Stream stream = webClient.OpenRead(url);

                // Save the image from the stream
                return Image.FromStream(stream);
            }
            
            return null;
        }
       
    }
}
