﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTracker
{
    /// <summary>
    /// Convert.
    ///     Contains function for converting between different data types.
    public class Convert
    {
        /// <summary>
        /// Convert a String value to an integer value.
        /// </summary>
        public static int StringToInt(String value)
        {
            if ((value == String.Empty) || (value == null))
            {
                return 0;
            }

            try
            {
                return int.Parse(value);
            }
            catch (Exception) { }

            return 0;
        }
        
        /// <summary>
        /// Convert a String value to an integer value.
        /// </summary>
        public static Decimal StringToDecimal(String value)
        {
            if ( (value == String.Empty) || (value == null ) )
            {
                return 0;
            }

            try
            {
                return Decimal.Parse(value);
            }
            catch(Exception) { }

            return 0;
        }

        /// <summary>
        /// Convert a List<String> to a String, separated by commas.
        /// </summary>
        public static String ListToString(List<String> list)
        {
            if (list == null)
            {
                return String.Empty;
            }
            else
            {
                return string.Join("; ", list.ToArray());
            }
        }
    }
}
