﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace MovieTracker.Utils
{

    /// <summary>
    /// A collection of useful static functions.
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// Resize an image, while maintaining aspect ratio.
        /// </summary>
        /// <param name="src">Image to resize.</param>
        /// <param name="width">The new width of the image.</param>
        /// <returns>The resized image.</returns>
        public static Image ResizeImage(Image src, int width)
        {
            if (src == null) return null;

            float percent = (float)src.Height / (float)src.Width;

            // Calculate new height of the image.
            int height = (int)(percent * width);

            // Resize the image.
            Image image = new Bitmap(src, width, height);

            return image;
        }

        /// <summary>
        /// Create a thumbnail image with a border that can be displayed in the ListView control.
        /// </summary>
        /// <param name="image"> The image from which create the thumbnail.</param>
        /// <param name="width">Width of the thumbnail.</param>
        /// <param name="height">Height of the thumbnail.</param>
        /// <param name="padding">The space between the border and the image </param>
        /// <returns>The thumbnail image with a border.</returns>
        public static Image CreateThumbnail(Image image, int width, int height, int padding)
        {
            if (image == null) return null;

            Bitmap thumbnail = new Bitmap(width, height);

            // Resize image while maintanning aspect ratio.
            image = Utility.ResizeImage(image, width - padding * 2);


            using (Graphics graphics = Graphics.FromImage(thumbnail))
            {
                // Set clipping area so the image is not drawn over the padding.
                graphics.SetClip(new Rectangle(padding, padding,
                    width - padding * 2, height - padding * 2));

                // If the height of the resized image is smaller then the height available
                // draw the image in the middle.
                if (image.Height < (height - padding * 2))
                {
                    // Draw the image vertically aligned to the center.
                    graphics.DrawImage(image, padding, (height - image.Height) / 2);
                }
                else
                {
                    graphics.DrawImage(image, padding, padding);
                }

                graphics.ResetClip();

                // Draw the border of the thumbnail.
                graphics.DrawRectangle(Pens.LightGray, 0, 0, width - 1, height - 1);
            }

            return thumbnail;
        }
        
        /// <summary>
        /// Displays a message box with an error message.
        /// </summary>
        /// <param name="errorMessage">The error message to display.</param>
        public static void ShowErrorMessage(string errorMessage)
        {
            MessageBox.Show(null, errorMessage, "MovieTracker - Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Displays a message box with a message.
        /// </summary>
        /// <param name="errorMessage">The message to display.</param>
        public static void ShowMessage(string errorMessage)
        {
            MessageBox.Show(null, errorMessage, "MovieTracker", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

    }
}
