using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

/// <summary>
/// Responsable for parsing imdb for information about movies.
/// </summary>
class Script 
{
    public string Name
    {
        get { return "All Movie"; }
    }

	public string SearchPage
    {
        get { return "http://www.allmovie.com/search/all/"; }
    }
    

    /// <summary>
    /// List of movie urls found after parsing the calling searchPage method.
    /// </summary>
    public List<String> URLList 
	{
		get { return this.urlList;	}
	}

    private List<String> urlList = new List<String>();
    

    /// <summary>
    /// List of movie titles found after parsing the calling searchPage method.
    /// </summary>
    public List<String> TitleList 
	{
        get { return this.titleList; }
	}

    private List<String> titleList = new List<String>();
    

    /// <summary>
    /// List of movie descriptions found after parsing the calling searchPage method.
    /// </summary>    
    public List<String> DescriptionList 
	{
		get { return this.descriptionList; }
	}

    private List<String> descriptionList = new List<String>();
     
        
    /// <summary>
    /// Title of the movie.
    /// </summary>
    public String Title
    {
        get { return this.title;  }
    }

    private String title = String.Empty;
    

    /// <summary>
    /// The year the movie was released.
    /// </summary>
    public String Year
    {
        get { return this.year;  }
    }

    private String year = String.Empty;

    
    /// <summary>
    /// The genres of the movie.
    /// </summary>
    public List<String> Genres 
    {
        get { return this.genres;  }
    }

    private List<String> genres = new List<String>();
    

    /// <summary>
    /// The description of the movie.
    /// </summary>
    public String Description
    {
        get { return this.description;  }
    }

    private String description = String.Empty;


    /// <summary>
    /// The directors of the movie
    /// </summary>
    public List<String> Directors
    {
        get { return this.directors;  }
    }

    private List<String> directors = new List<string>();


    /// <summary>
    /// The URL to the poster of the movie.
    /// </summary>
    public String PosterURL
    {
        get { return this.posterURL;  }
    }

    private String posterURL = "";

	/// <summary>
    /// The directors of the movie
    /// </summary>
    public List<String> Cast
    {
        get { return this.cast;  }
    }

    private List<String> cast = new List<string>();
	
	
	/// <summary>
    /// The year the movie was released.
    /// </summary>
    public String Rating
    {
        get { return this.rating;  }
    }

    private String rating = String.Empty;

    /// <summary>
    /// Parse the search page.
    /// </summary>
    /// <param name="webPage">The search page to parse.</param>
    public void parseSearchPage(string webPage)
	{
        // Clear previous search information.
        this.resetSearchInfo();
		
		// Remove new line charachter from the website, makes parsing somewhat easier.
        webPage = webPage.Replace("\n", "");

        // Parse the search page.
		String linkPattern = @"<div class=""info"">(\s+)<h4>Movie</h4>(\s+)<div class=""title"" data-tooltip=""(.*?)"">"
                + @"(\s+)<a href=""" + "(.*?)" + @""">" + "(.*?)" + @"</a>(.*?) </div>";

        // Foreach result on the search page.
        foreach (Match match in new Regex(linkPattern).Matches(webPage))
        {
            // The URL to the page of the movie.
            urlList.Add(match.Groups[5].Value);

            //  The title of the movie.
	        titleList.Add(match.Groups[6].Value);
			 
            // The description of the movie.
            descriptionList.Add(match.Groups[7].Value);
         }
	}

    /// <summary>
    /// Clear the search result.
    /// </summary>
    private void resetSearchInfo()
    {
        this.urlList.Clear();
        this.titleList.Clear();
        this.descriptionList.Clear();
    }

    /// <summary>
    /// Parse the movies web page.
    /// </summary>
    /// <param name="webPage">The movies page to parse.</param>
    public void parseMoviePage(string webPage)
	{
        this.resetMovieInfo();

        // Remove new line charachter from the website, makes parsing somewhat easier.
        webPage = webPage.Replace("\n", "");
        



        // Parse the movie title.
        String titlePattern = @"<h2 class=""movie-title"" itemprop=""name"">(.*?)<span";

        this.title = new Regex(titlePattern).Match(webPage).Groups[1].Value;

        // Remove white spaces from the end and the begging.
        this.title = this.title.Trim();
        


        // Parse the movies releas year.
        String yearPattern = @"<span class=""release-year"">\((.*?)\)</span>";

        this.year = new Regex(yearPattern).Match(webPage).Groups[1].Value;
        


        // Parse the movies genre.
        String genresPattern = @"<span class=""header-movie-genres"">(\s+)Genres - " +
                @"<a href=""(.*?)"">(.*?)</a>";

        foreach (Match match in new Regex(genresPattern).Matches(webPage))
        {
            genres.Add(match.Groups[3].Value);
        }
        


        // Parse the description of the movie.
        String descriptionPattern = @"<div class=""text"" itemprop=""description"">" + "(.*?)" + @"</div>";

        this.description = new Regex(descriptionPattern).Match(webPage).Groups[1].Value.Trim();

        this.description = this.RemoveLinksFromText(this.description);



        // Parse the directors of the movie.
        String directorsPattern = @"<h3 class=""movie-director"" id=""movie-director-link"""
                    + @"(\s+)itemprop=""director""" + @"(\s+)itemscope itemtype=""http://schema.org/Person"">"
                    + @"(\s+)Directed by <span itemprop=""name"">" + "(.*?)" + @"</h3>";

        String _directors = new Regex(directorsPattern).Match(webPage).Groups[4].Value;

        // Remove links from text.
        _directors = this.RemoveLinksFromText(_directors);

        // There might be multiple directors separated by a slash.
        this.directors.AddRange(_directors.Split('/'));

        // Remove white spaces.
        for(int i = 0; i< directors.Count; i++) 
        {
            directors[i] = directors[i].Trim();
        }

        

        // Parse link of the movies poster.
        String posterPattern = @"<div class=""poster"">(\s+)<img src=""(.*?)""";

        this.posterURL = new Regex(posterPattern).Match(webPage).Groups[2].Value;
    }

    /// <summary>
    /// Clear parse information.
    /// </summary>
    private void resetMovieInfo()
    {
        this.title = String.Empty;
        this.year = String.Empty;
        this.genres.Clear();
        this.description = String.Empty;
        this.directors.Clear();
        this.posterURL = String.Empty;
    }

    /// <summary>
    /// Removes links from the specified string.
    /// </summary>
    private String RemoveLinksFromText(string text)
    {
        bool removedlinks = false;

        while (removedlinks == false)
        {
            if (text.IndexOf("<") > -1)
            {
                // Find the beginning and the end of the tag and remove it
                int startpos = text.IndexOf("<");
                int endpos = text.IndexOf(">");

                text = text.Remove(startpos, endpos - startpos + 1);
            }
            else  removedlinks = true;
        }

        return text;
    }

}