using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

/// <summary>
/// 
/// </summary>
class Script 
{
	public string Name
    {
        get { return "Imdb"; }
    }
	
	public string SearchPage
    {
        get { return "http://www.imdb.com/find?s=tt&q="; }
    }
    

    /// <summary>
    /// List of movie urls found after parsing the calling searchPage method.
    /// </summary>
    public List<String> URLList 
	{
		get { return this.urlList;	}
	}

    private List<String> urlList = new List<String>();
    

    /// <summary>
    /// List of movie titles found after parsing the calling searchPage method.
    /// </summary>
    public List<String> TitleList 
	{
        get { return this.titleList; }
	}

    private List<String> titleList = new List<String>();
    

    /// <summary>
    /// List of movie descriptions found after parsing the calling searchPage method.
    /// </summary>    
    public List<String> DescriptionList 
	{
		get { return this.descriptionList; }
	}

    private List<String> descriptionList = new List<String>();
     
        
    /// <summary>
    /// Title of the movie.
    /// </summary>
    public String Title
    {
        get { return this.title;  }
    }

    private String title = String.Empty;
    

    /// <summary>
    /// The year the movie was released.
    /// </summary>
    public String Year
    {
        get { return this.year;  }
    }

    private String year = String.Empty;

    
    /// <summary>
    /// The genres of the movie.
    /// </summary>
    public List<String> Genres 
    {
        get { return this.genres;  }
    }

    private List<String> genres = new List<String>();
    

    /// <summary>
    /// The description of the movie.
    /// </summary>
    public String Description
    {
        get { return this.description;  }
    }

    private String description = String.Empty;


    /// <summary>
    /// The directors of the movie
    /// </summary>
    public List<String> Directors
    {
        get { return this.directors;  }
    }

    private List<String> directors = new List<string>();


    /// <summary>
    /// The URL to the poster of the movie.
    /// </summary>
    public String PosterURL
    {
        get { return this.posterURL;  }
    }

    private String posterURL = "";
	
	
	/// <summary>
    /// The directors of the movie
    /// </summary>
    public List<String> Cast
    {
        get { return this.cast;  }
    }

    private List<String> cast = new List<string>();
	
	
	/// <summary>
    /// The year the movie was released.
    /// </summary>
    public String Rating
    {
        get { return this.rating;  }
    }

    private String rating = String.Empty;


    /// <summary>
    /// Parse the search page.
    /// </summary>
    /// <param name="webPage">The search page to parse.</param>
    public void parseSearchPage(string webPage)
	{
        // Clear previous search information.
        this.resetSearchInfo();

        // Parse the search page.
		String linkPattern = @"<td class=""result_text""> <a href=""/title/tt" + @"(\d{7})" +
                "/\\?ref_=fn_tt_tt"+ "(.*?)" + ">" + "(.*?)" + "</a>" + "(.*?)"+ "</td>";

        // Foreach result on the search page.
        foreach (Match match in new Regex(linkPattern).Matches(webPage))
        {
            // The URL to the page of the movie.
            urlList.Add("http://www.imdb.com/title/tt" + match.Groups[1].Value);

            //  The title of the movie.
	        titleList.Add(match.Groups[3].Value);
			 
            // The description sometimes contains a link to an episode, which is not needed,
            // so we remove it.
			String description = match.Groups[4].Value;
			 
			int index = description.IndexOf("<br/>");
			 
            if (index != -1) 
			{	 
			    description =  description.Remove(index);
			}
			 
            descriptionList.Add(description);
         }
	}

    /// <summary>
    /// Clear the search result.
    /// </summary>
    private void resetSearchInfo()
    {
        this.urlList.Clear();
        this.titleList.Clear();
        this.descriptionList.Clear();
    }

    /// <summary>
    /// Parse the movies web page.
    /// </summary>
    /// <param name="webPage">The movies page to parse.</param>
    public void parseMoviePage(string webPage)
	{
        this.resetMovieInfo();

        // Remove new line charachter from the website, makes parsing somewhat easier.
        webPage = webPage.Replace("\n", "");
        

        // Parse the movie title.
        String titlePattern = @"<h1 itemprop=""name"" class="""">" + "(.*?)" + "&nbsp;";

        this.title = new Regex(titlePattern).Match(webPage).Groups[1].Value;


        // Parse the movies releas year.
        String yearPattern = @"<span id=""titleYear"">\(<a href=""/year/" + "(.*?)" + @"/\?ref?";

        this.year = new Regex(yearPattern).Match(webPage).Groups[1].Value;


        // Parse the movies genre.
        String genresPattern = @"<span class=""itemprop"" itemprop=""genre"">" + "(.*?)" + "</span></a>";

        foreach (Match match in new Regex(genresPattern).Matches(webPage))
        {
            genres.Add(match.Groups[1].Value);
        }
        

        // Parse the description of the movie.
        String descriptionPattern = @"<div class=""summary_text"" itemprop=""description"">" + "(.*?)" + @"<";

        this.description = new Regex(descriptionPattern).Match(webPage).Groups[1].Value.Trim();


        // Parse the directors of the movie.
        String directorsPattern = @"<span itemprop=""director"" itemscope itemtype=""http://schema.org/Person"">"
                + @"<a href=""/name/(.*?)ref_=tt_ov_dr""" + @"itemprop='url'><span class=""itemprop"" itemprop=""name"">"
                + "(.*?)" + "</span></a>";
        
        foreach (Match match in new Regex(directorsPattern).Matches(webPage))
        {
            directors.Add(match.Groups[2].Value);
        }
        

        // Parse link of the movies poster.
        String posterPattern = @"<div class=""poster""><a href=""/title/(.*?)ref_=tt_ov_i"">"
                + @" <img alt=""(.*?)"" title=""(.*?) Poster""src=""" + @"(.*?)""";

        this.posterURL = new Regex(posterPattern).Match(webPage).Groups[4].Value;
				
				
		
        // Parse cast of the movie
        String castPattern = @"<span itemprop=""actors"" itemscope itemtype=""http://schema.org/Person"">"
						+@"<a href=""(.*?)"""
						+@"itemprop='url'><span class=""itemprop"" itemprop=""name"">(.*?)</span></a>";

		foreach (Match match in new Regex(castPattern).Matches(webPage))
        {
            cast.Add(match.Groups[2].Value);
        }

		// Parse the rating of the movie.
		String ratingPattern = @"<span itemprop=""ratingValue"">(.*?)</span>";

        this.rating = new Regex(ratingPattern).Match(webPage).Groups[1].Value;
		
		
		
		
    }

    /// <summary>
    /// Clear parse information.
    /// </summary>
    private void resetMovieInfo()
    {
        this.title = String.Empty;
        this.year = String.Empty;
        this.genres.Clear();
        this.description = String.Empty;
        this.directors.Clear();
        this.posterURL = String.Empty;
    }

}